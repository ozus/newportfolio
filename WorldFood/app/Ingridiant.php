<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingridiant extends Model
{
    protected $table = 'ingridiants';
    public $timestamps = false;


    public function meals() {
    	return $this -> belongsTo('App\Meal', 'meal_id');
    }
}
