<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $table = 'meals';
    public $timestamps = false;

    public function categories() {
    	return $this -> belongsTo('App\Category', 'cat_id');
    }

    public function tags() {
    	return $this -> hasMany('App\Tag', 'meal_id', 'id');
    }

    public function ingridians() {
    	return $this -> hasMany('App\Ingridiant', 'meal_id', 'id');
    }

    public function scopeFilterByCategory($query, $cat_id=NULL) {
        if($cat_id == 'all') {
            return $query -> whereHas('categories', function($q) use ($cat_id) {
                $q -> where('id', '>', 0);
            }); 
        }
        elseif($cat_id == 'n') {
            return $query -> where('cat_id', '=', NULL); 
        }
        else {
            return $query -> whereHas('categories', function($q) use ($cat_id) {
                $q -> where('id', '=', $cat_id);
            }); 
        }
    }

    public function scopeFilterByTag($query, $tags_id=NULL) {
        if(isset($tags_id)) {
            return $query -> whereHas('tags', function($q) use ($tags_id) {
                $q -> whereIn('id', $tags_id);
            });
        } else {
            return $query;
        }
    }

    public function scopeFilterByTimeDiff($query, $diff=NULL) {
        if($diff) {
            return $query -> where('created_at', '>', $diff) -> orWhere('updated_at', '>', $diff) -> orWhere('deleted_at', '>', $diff);
        } else {
            return $query;
        }    
    }
}
