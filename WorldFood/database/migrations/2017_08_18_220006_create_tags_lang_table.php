<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_lang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id');
            $table->string('en_title', 45)->nullable();
            $table->string('en_desc', 1000)->nullable();
            $table->string('fr_title', 45)->nullable();
            $table->string('fr_desc', 1000)->nullable();
            $table->string('de_title', 45)->nullable();
            $table->string('de_desc', 1000)->nullable();
            $table->string('hr_title', 45)->nullable();
            $table->string('hr_desc', 1000)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_lang');
    }
}
