<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MealsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(IngridiantsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(CategoryLangTableSeeder::class);
    	$this->call(MealsLangTableSeeder::class);
        $this->call(IngridiantsLangTableSeeder::class);
    	$this->call(TagsLangTableSeeder::class);
    	

    }
}
