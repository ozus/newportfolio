<?php

use Illuminate\Database\Seeder;
use App\Meal as Meal;

class MealsTableSeeder extends Seeder
{
	protected $faker;

	public function __construct(Faker\Generator $faker) {
		$this -> faker = $faker;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach(range(1,20) as $index) {
            $meal = new Meal();
        	$meal->id=$index;
        	$meal->lang_id = 21 - $index;
        	if($index < 16) {
        		$meal->cat_id = $this->faker->numberBetween($min = 1, $max = 5);
        	}
        	$meal->created_at = $this->faker->unixTime($max = 'now');
        	$meal->updated_at = $this->faker->unixTime($max = 'now');
            if($meal->created_at < $meal->updated_at) {
                $meal->status = "modified";
            } else {
                $meal->status = "created";
            }
        	if($index == 2 || $index == 4 || $index == 6 || $index == 8 || $index == 15) {
        		$meal->deleted_at = $this->faker->unixTime($max = 'now');
                $meal->status = "deleted";
        	}
        	$meal->slug = $this->faker->slug;
        	$meal->save();

        }


    }
}
