<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>World Food - @yield('title')</title>
		{{Html::style('css/bootstrap/bootstrap.css')}}
		{{Html::style('css/custom.css')}}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="master-content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	
	
	
	{{Html::script('js/bootstrap/bootstrap.min.js')}}
	
	
	</body>
</html>
