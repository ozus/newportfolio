@extends('layouts.master')

@section('title')
Nothing found
@endsection

@section('content')
	<div class="no-results">
		No results maching search criteria found
	</div>
	<a class="back-btn" href="{{url('/')}}" >Back</a>
@endsection