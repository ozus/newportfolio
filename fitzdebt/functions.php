<?php

//editing java scripts
function scripts_settings() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', '');
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'scripts_settings');

//defining TEMPLATE_URI cause this path is used a lot
define('TEMPLATE_URI', get_template_directory_uri());
define('TEMPLATE_PATH', get_template_directory());

//including ajax stuff
include_once TEMPLATEPATH . '/ajax.php';

function get_page_url($name) {
    $page = get_page_by_title($name);
    return get_permalink($page->ID);
}



function get_page_id($name) {
    $page = get_page_by_title($name);
    if (empty($page)) {
        $page = get_page_by_path($name);
        if (empty($page)) {
            return null;
        }
    }
    return $page->ID;
}



//function load_part($title) {
//    if ($rwd) {
//        if (is_file(TEMPLATEPATH . '/rwd-' . $title . '.php') && file_exists(TEMPLATEPATH . '/rwd-' . $title . '.php')) {
//            get_template_part('rwd', $title);
//        }
//    } else if (!$rwd) {
//        if (is_file(TEMPLATEPATH . '/page-' . $title . '.php') && file_exists(TEMPLATEPATH . '/page-' . $title . '.php')) {
//            get_template_part('page', $title);
//        }
//    }
//}

/**
 * comparing function and returning certain string
 * @param string $source
 * @param string $target
 * @param string $return
 * @return string
 */
function compare_and_return($source, $target, $return) {
    return ($source == $target) ? $return : '';
}

/**
 * including stuff
 */
include_once(TEMPLATE_PATH . '/lib/redux/framework.php');
include_once(TEMPLATE_PATH . '/lib/redux/config.php');

//define('ACF_LITE', true); //hiding ACF from admin panel after developement
include_once(TEMPLATE_PATH . '/lib/acf/acf.php');
include_once(TEMPLATE_PATH . '/lib/acf-repeater/acf-repeater.php');
include_once(TEMPLATE_PATH . '/lib/create-acf.php');



if (is_admin()) {
    include_once(TEMPLATE_PATH . '/lib/create-list-admin.php');
    include_once(TEMPLATE_PATH . '/lib/create-functions-admin.php');
}


/**
 * api used can be found on https://github.com/maxmind/geoip-api-php
 * database used can be found on http://dev.maxmind.com/geoip/geoip2/geolite2/
 * function checks ip and returns best guess for country based on database. can be
 * modified to return more detailed stuff like cities
 * @param string $ip ip that needs to be checked
 * @return array array that contains coutry code and name
 */
function getCountry($ip) {
    include_once(TEMPLATE_PATH . '/lib/geoip/src/geoip.inc');
    $gi = geoip_open(TEMPLATE_PATH . '/lib/geoip/GeoIP.dat', GEOIP_STANDARD);
    $country = array();
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false) {
        $country['code'] = geoip_country_code_by_addr($gi, $ip);
        $country['name'] = geoip_country_name_by_addr($gi, $ip);
    }
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false) {
        $country['code'] = geoip_country_code_by_addr_v6($gi, $ip);
        $country['name'] = geoip_country_name_by_addr_v6($gi, $ip);
    }
    geoip_close($gi);
    return $country;
}




add_image_size( 'investors-main', 457, 305 );
add_image_size( 'investors-bottom', 107, 107 );


function add_categories() {
    register_taxonomy_for_object_type('post_tag', 'page');
    register_taxonomy_for_object_type('category', 'page');
}

add_action('init', 'add_categories');

/**
 * enabling fetured image in posts
 */
add_theme_support('post-thumbnails');


add_theme_support( 'custom-background' );