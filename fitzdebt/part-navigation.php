<div id="navigation" class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><h1><img src="<?php echo TEMPLATE_URI; ?>/images/page/logo_nav.png" alt="<?php bloginfo('name'); ?>" /></h1></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul id="nav" class="nav navbar-nav">
                <li class="active"><a href="javascript:void(0);" data-target="#home">Home</a></li>
                <li class=""><a href="javascript:void(0);" data-target="#about-us">About Us</a></li>
                <li class=""><a href="javascript:void(0);" data-target="#investors">Investors</a></li>
                <li class=""><a href="javascript:void(0);" data-target="#services">Services</a></li>
                <li class=""><a href="javascript:void(0);" data-target="#contact">Contact</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
</div>