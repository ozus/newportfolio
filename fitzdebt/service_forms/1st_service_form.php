<?php
/**
 * Template Name: 1st Service Form
 */
?>

<?php acf_form_head(); ?>
<?php get_header(); ?>
<?php get_template_part( 'service_forms/services_forms_navigation' ); ?>
<?php while( have_posts() ) : the_post(); ?>
          <?php acf_form( array( 
               'post_id'	=> get_page_id( 'Service Page 1' ),
	       'post_title'	=> false,
	       'submit_value'	=> 'Update Service Page 1',
               'updated_message' => 'Service page 1 was created or updated'
           )); ?>
          <?php endwhile; ?>
<?php get_template_part( 'service_forms/services_forms_navigation' ); ?>
<?php get_footer(); ?>
           
           
