<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\BirthdayMail;

class SomeTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'some:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$users = User::select('created_at', 'email', 'name') -> get();
    	foreach($users as $user) {
    		$pices = explode(' ', $user -> created_at);
    		$datePices = explode('-', $pices[0]);
    		
    		if(Carbon::now() -> month == $datePices[1]) {
    			Mail::to($user -> email) -> send(new BirthdayMail($user));
    		}
    	}
        
    }
}
