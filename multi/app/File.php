<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';
    public $timestamps = false;
    
    public function user() {
    	
    	return $this -> belongsTo('App\User', 'usr_id');
    	
    }
    
    
    public function comment() {
    	
    	return $this -> hasMany('App\Comment');
    	
    }
}
