<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Carbon\Carbon;
use Auth;

class CommentController extends Controller
{
    
	
	public function addComment(Request $request) {
		
		$comm = $request -> input('comment');
		if($request -> has('name')){
			$author = $request -> input('name');
		} else {
			$author = Auth::user() -> name;
		}
		
		if(!Comment::where('author', '=', $author) -> where('comment', '=', $comm) -> exists()) {
			$comment = new Comment();
			
			if($request -> has('name')) {
				$comment -> author = $request -> input('name');
			} else {
				$comment -> author = Auth::user() -> name;
				$comment -> usr_id = Auth::user() -> id;
			}
			
			$comment -> comment = $request -> input('comment');
			$comment -> file_id = $request -> input('ID');
			
			$comment -> time_posted = Carbon::now() -> toTimeString();
			$comment -> date_posted = Carbon::now() -> toDateString();
			
			if($comment -> save()) {
				$request -> session()-> flash('commSuccess', 'Comment successflully added');
				return back();
			} else {
				$request -> session() ->flash('commError', 'Comment could nto be added');
				return back();
			}
		} else {
			$request -> session() -> flash('commError', 'Comment already added');
			return back();
		}
	}
	
	public function deleteComment(Request $request, $id = null) {
		
		if(Comment::where('id', '=', $id) -> exists()) {
			Comment::where('id', '=', $id) -> delete();
			
			$request -> session() -> flash('commSuccess', 'Comment is deleted');
			return back();
		} else {
			$request -> session() -> flash('commError', 'Comment could not be deleted');
			return back();
		}
		
	}
	
	
	public function getEditComment($id = null) {
		$comm = Comment::select('comment') -> where('id', '=', $id) -> get();
		
		return view('comments.edit', ['comm' => $comm, 'id' => $id]);
	}
	
	public function editComment(Request $request) {
		
		if(Comment::where('id', '=', $request -> input('id')) -> exists()) {
			Comment::where('id', '=', $request -> input('id')) -> update(['comment' => $request -> input('comment')]);
			
			$request -> session() -> flash('success', 'Comment was successfully updated');
			return back();
		} else {
			$request -> session() -> flash('error', 'Comment could not be updated');
			return back();
		}
		
	}
	
}
