<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DocumentController extends Controller
{
    public function showDocs() {
    
    	$docs = DB::select(DB::raw("SELECT * FROM files WHERE path LIKE '%documents%'"));
    	
    	return view('pages.files') -> with('docs', $docs);

	}
}
