<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File as Fil;
use DB;
use Auth;

class ImageController extends Controller
{
    public function getImage() {
    	
			$images = DB::table('files') -> select("*") -> where('path', 'LIKE', '%images%') -> paginate(3);
			$viewed = Fil::where('path', 'LIKE', '%images%') -> where('viewed', '>', '5') -> limit(5) -> get();
			return view('pages.image', ['images' => $images, 'viewed' => $viewed]);
    	
    		
    
	}
	
	public function showImage($id = null) {
		
		$image = Fil::where('id', '=', $id) -> get();
		
		$image[0] -> viewed = $image[0] -> viewed + 1;
		
		$image[0] -> save();
		
		
		
		$viewed = Fil::where('path', 'LIKE', '%images%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
		$files = Fil::where('path', 'LIKE', '%images%') -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
		
		$images = $filesFiltered -> flatten();
		//$images = Fil::where('id', '=', $id) -> get();
		
		return view('pages.showImg', ['image' => $images, 'viewed' => $viewed, 'files' => $files]);
		
	}
	
	
	
	public function ajaxLike($id = null) {
	
		//return "ID = " . $id;
	
		$like = Fil::where('id', '=', $id) -> get();
	
		$like[0] -> liked = $like[0] -> liked + 1;
	
		$like[0] -> save();
	
		echo "<button type='button' id='unlikeButt' class='btn btn-default' onclick='unLike();'>Unlike Image</button>";
		echo "  ";
		echo "Image liked";
	
	
	
	}
	
	
	
	public function ajaxUnlike($id = null) {
	
		//return "ID = " . $id;
	
		$like = Fil::where('id', '=', $id) -> get();
	
		$like[0] -> liked = $like[0] -> liked - 1;
	
		$like[0] -> save();
	
		echo "<button type='button' id='likeButt' class='btn btn-default' onclick='like();'>Like Image</button>";
	
	
	}
	
	
	
	public function ajaxDislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = Fil::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked + 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='unDislikeButt' class='btn btn-default' onclick='unDislike();'>Remove dislike</button>";
		echo "  ";
		echo "Image disliked";
	
	
	}
	
	
	public function ajaxUndislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = Fil::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked - 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='dislikeButt' class='btn btn-default' onclick='dislike();'>Dislike Image</button>";
	
	}
	
	
	
	public function ajaxImage($pag = null) {
		
		$images = DB::table('files') -> select("*") -> where('path', 'LIKE', '%images%') -> paginate($pag);
		$viewed = Fil::where('path', 'LIKE', '%images%') -> where('viewed', '>', '5') -> limit(5) -> get();
		return view('pages.ajax.images', ['images' => $images, 'viewed' => $viewed]);
		//return "ajax is working";
	}
	
	
	public function ajaxPref($value = null) {
	
		if($value == "viewed") {
			$title = "Most viewed";
			$viewed = Fil::where('path', 'LIKE', '%images%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
				
		}
	
		if($value == "liked") {
			$title = "Most Liked";
			$viewed = Fil::where('path', 'LIKE', '%images%') -> where('liked', '>', '1') -> orderBy('liked') -> limit(5) -> get();
				
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
	
		if($value == "disliked") {
			$title = "Most Disliked";
			$viewed = Fil::where('path', 'LIKE', '%images%') -> where('disliked', '>', '1') -> orderBy('disliked')-> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
	
		if($value == "favorites") {
				
			if(Auth::check()) {
	
				$viewed = Auth::user() -> favorits;
				
				$filteredFavs = $viewed -> filter(function($value, $key) {
					return str_contains($value -> path, 'images');
				});
				
				$return = $filteredFavs -> take(1);
	
				return view('pages.ajax.videoPref', ['viewed' => $return]);
	
			}
		}
	
	}
}
