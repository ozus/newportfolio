<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
	//use AuthenticatesUsers;
	
    public function __construct() {
    	$this -> middleware('guest', ['except' => 'logout']);
	}
	
	public function getLogin() {
		
		return view('auth.login');
		
	}
	
	public function postLogin(Request $request) {
		
		$this -> validate($request, [
				
			'email' => 'required',
			'password' => 'required',	
				
		]);
		
		$email = $request -> email;
		$password = $request -> password;
		
		if(Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1], $request -> remember)) {
			return redirect() -> route('home'); 
		}  else {
			return redirect() -> route('login') -> withInput() -> withErrors( [
				'email' => 'We could not log you in, peleas check you user name and/or password. Maybe you did not activate your account?',
			]);
		} 
		
	}
	
	public function logout() {
		
		if(Auth::check()) {
			Auth::logout();
			return redirect() -> route('home');
		}
		
	}
}
