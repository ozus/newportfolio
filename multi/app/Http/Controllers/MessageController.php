<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    
	
	public function inbox() {
		
		$messages = DB::connection('mysql') -> table('messages') -> select('*') -> where('to', '=', Auth::user() -> name) 
		-> orderBy('recived_at', 'desc')
		-> get();
		
		$decodedMessages = $this -> messageDecode($messages);
		
		$shortmMessages = $this -> makeShortMessage($decodedMessages); 
		
		//dd($shortmMessages);
		
		$dateTime = $this -> formatDateTime($messages, 'recived_at');
		
		$senders = $this -> getSenders($messages);
		
		return view('usrpages.messaging.inbox', ['messages' => $messages, 'short' => $shortmMessages, 'dt' => $dateTime, 'senders' => $senders]);
		
	}
	
	public function sent() {
		
		$messages = DB::connection('mysql') -> table('messages') -> select('*') -> where('from', '=', Auth::user() -> name)
		-> orderBy('recived_at', 'desc')
		-> get();
		
		$decodedMessages = $this -> messageDecode($messages);

		$shortmMessages = $this -> makeShortMessage($decodedMessages);
		
		$dateTime = $this -> formatDateTime($messages, 'send_at');
		
		$recivers = $this -> getRecivers($messages);
		
		return view('usrpages.messaging.sent', ['messages' => $messages, 'short' => $shortmMessages, 'dt' => $dateTime, 'recivers' => $recivers]);
		
	}
	
	
	public function read($id = NULL, $mode = NULL) {
		
		DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> update([$mode => 1]);
		
		$message = DB::connection('mysql') -> table('messages') -> select('*') -> where('id', '=', $id) -> get();
		
		$decodedMessage = $this -> messageDecode($message);
		
		return view('usrpages.messaging.read', ['message' => $message, 'decoded' => $decodedMessage, 'mode' => $mode]);
		
	}
	
	public function ajaxDelete($id = NULL, $mode = NULL) {
		if($message = DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> get()) {
			if(DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> update([$mode => 0])) {
				echo "Message was successfully deleted";
			} else {
				echo "Message could not be deleted";
			}
			
		}
	}
	
	
	public function reply($send = NULL) {
		Log::info("Working");
		$input = json_decode($send);
		//Log::info('text' . $input);
		$messageForSave = urlencode($strbase64_decode($input[3]));
		//return json_encode($input[3]);
		if(DB::connection('mysql') -> table('messages') -> insert([
			'title' => $input[2],
			'message' => $messageForSave,
			'from' => $input[0],
			'to' => $input[1],
			'send_at' => Carbon::now(),
			'recived_at' => Carbon::now(),		
		])) {
			return "Replay sent";
		} else {
			return "Reply could not be send";
		}
		 
	}
	
	public function deleteSingle($id = NULL, $mode = NULL) {
		
		if($message = DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> get()) {
			if(DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> update([$mode => 0])) {
				Request::session() -> flash('success', "Message was successfully deleted");
				return redirect() -> route('messages.inbox');
			} else {
				Request::session() -> flash('error', "Message could not be deleted");
				return back();
			}
		}
		
	}
	
	
	public function ajaxTrash($id = NULL, $mode = NULL) {
		if($message = DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> get()) {
			if(DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> update([$mode => 1])) {
				echo "Message moved to trash";
			} else {
				echo "Could not move message to trash";
			}
		}
	}
	
	
	public function trashSingle($id = NULL, $mode = NULL) {
	
		if($message = DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> get()) {
			if(DB::connection('mysql') -> table('messages') -> where('id', '=', $id) -> update([$mode => 1])) {
				Request::session() -> flash('success', "Message sucsfully moved to trash");
				return back();
			} else {
				Request::session() -> flash('error', "Message could not be moved to trash");
				return back();
			}
		}
	
	}
	
	public function ajaxGetSenderMessages($sender = NULL) {
		
		if($sender == "all") {
			$messages = DB::connection('mysql') -> table('messages') -> select('*') -> where('to', '=', Auth::user() -> name)
			-> orderBy('recived_at', 'desc')
			-> get();
		} else {
			$messages = DB::connection('mysql') -> table('messages') -> where('from', '=', $sender) 
			-> orderBy('recived_at', 'desc') 
			-> get();
		}
	
		$decodedMessages = $this -> messageDecode($messages);
		
		$shortmMessages = $this -> makeShortMessage($decodedMessages);
		
		$dateTime = $this -> formatDateTime($messages, 'recived_at');
		
		return view('pages.ajax.messagesBySender', ['messages' => $messages, 'short' => $shortmMessages, 'dt' => $dateTime]);
		
		
	}
	
	
	public function ajaxGetReciverMessages($reciver = NULL) {
	
		if($reciver == "all") {
			$messages = DB::connection('mysql') -> table('messages') -> select('*') -> where('from', '=', Auth::user() -> name)
			-> orderBy('recived_at', 'desc')
			-> get();
		} else {
			$messages = DB::connection('mysql') -> table('messages') -> where('to', '=', $reciver)
			-> orderBy('recived_at', 'desc')
			-> get();
		}
	
		$decodedMessages = $this -> messageDecode($messages);
	
		$shortmMessages = $this -> makeShortMessage($decodedMessages);
	
		$dateTime = $this -> formatDateTime($messages, 'recived_at');
	
		return view('pages.ajax.messagesByReciver', ['messages' => $messages, 'short' => $shortmMessages, 'dt' => $dateTime]);
	
	
	}
	
	
	public function ajaxAddImage($img = NULL, $width = NULL, $height = NULL) {
		
		$ext = $img -> getClientOriginalExtension();		
		
		return $ext;
		
	}
	
	
	protected function messageDecode($mess) {
		
		$decoded = array();
		foreach($mess as $m) {
			$message = urldecode($m -> message);
			array_push($decoded, $message);
		}
		
		return $decoded;
		
	}
	
	protected function makeShortMessage($mess) {
		
		$returnMessages = array();
		
		//foreach($mess as $m) {
		for($i = 0; $i < count($mess); $i++) {
			
				$striped_mess = strip_tags($mess[$i]);
			
			//dd($striped_mess);
			if(strlen($striped_mess) > 130) {
				//dd($mess[$i]);
				//$message = wordwrap($m -> message, 130, '\xyz', false);
				$messs = substr($striped_mess, 0, 130) . "...";
				//$messageBold = wordwrap($m -> message, 123, '\xyz', false);
				$messBold = substr($striped_mess, 0, 122) . "....";
				$doc = new \DOMDocument();
				@$doc -> loadHTML($messs);
				$mess1 = $doc -> saveHTML();
				@$doc -> loadHTML($messBold);
				$messBold1 = $doc -> saveHTML();
				
				$return['message'] = $mess1;
				$return['messageBold'] = $messBold1;
			} else {
				$return['message'] = $striped_mess;
				$return['messageBold'] = $striped_mess;
			}
			array_push($returnMessages, $return);
		}
		//dd($returnMessages);
		return $returnMessages;
	}
	
	
	protected function formatDateTime($mess, $format) {
		
		$return = array();
		foreach($mess as $m) {
			$parts = explode(" ", $m -> $format);
			
			$partsDate = explode("-", $parts[0]);
			$partsTime = explode(":", $parts[1]);
			
			$dateTime = Carbon::create($partsDate[0], $partsDate[1], $partsDate[2], $partsTime[0], $partsTime[1], $partsTime[2]);
			
			$date = $dateTime -> toFormattedDateString();
			$formatedDate = substr($date, '0', strpos($date, ','));
			
			$time = $dateTime -> toTimeString();
			$formatedTime = substr($time, '0', strrpos($time, ":"));
			
			if($partsDate[1] != Carbon::now() -> month || $partsDate[2] != Carbon::now() -> day) {
				array_push($return, $formatedDate);
			} else {
				array_push($return, $formatedTime);
			}
		
			
		}
		
		return $return;
	}
	
	protected function getSenders($mess) {
		
		$senders = array();
		foreach($mess as $m) {
			if(!in_array($m -> from, $senders)) {
				array_push($senders, $m -> from);
			}
		}
		return $senders;
	}
	
	
	protected function getRecivers($mess) {
		
		$recivers = array();
		foreach($mess as $m) {
			if(!in_array($m -> to, $recivers)) {
				array_push($recivers, $m -> to);
			}
		}
		return $recivers;
		
	}
	
}


























