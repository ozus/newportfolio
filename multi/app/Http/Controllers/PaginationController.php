<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class PaginationController extends Controller
{
    
	public function prevNext($id = null, $type = null, $role = null) {
		//$idd == $id;
		
		$files = File::where('path', 'LIKE', "%" .$type . "%") -> get();
		$viewed = File::where('path', 'LIKE', "%" .$type . "%") -> where('viewed', '>', '5') -> limit(5) -> get();
	
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
	
	
			$keys = $filesFiltered -> keys();
			$key = $files -> search($filesFiltered[$keys[0]]);
				
			if($role == 'next') {	
				$lng = count($files);
				if($key == $lng - 1) {
					$fileReturn = $files -> get($key);
				} else {
					$fileReturn = $files -> get($key + 1);
				}
				
				$fileReturn -> viewed = $fileReturn -> viewed + 1;
				
				$fileReturn -> save();
			}
			
			if($role == 'prev') {
				
				if($key == 0) {
					$fileReturn = $files -> get($key);
				} else {
					$fileReturn = $files -> get($key - 1);
				}
				
				$fileReturn -> viewed = $fileReturn -> viewed + 1;
				
				$fileReturn -> save();
				
			}
	
			$array[0] = $fileReturn;
	
			$col = collect($array);
			
			
			switch ($type) {
				case 'images' :
					return view('pages.showImg', ['image' => $col, 'viewed' => $viewed, 'files' => $files]);
				break;
				case 'videos' :
					return view('pages.showVid', ['videos' => $col, 'viewed' => $viewed, 'files' => $files, 'chck' => 0]);	
				break;
				case 'music' :
					return view('pages.showMsc', ['musics' => $col, 'viewed' => $viewed, 'files' => $files, 'chck' => 0]);
				break;	
			} 
	
	}
	
	
	public function ajaxAuto($id = null, $type= null) {
		
		
		$files = File::where('path', 'LIKE', "%" .$type . "%") -> get();
		$viewed = File::where('path', 'LIKE', "%" .$type . "%") -> where('viewed', '>', '5') -> limit(5) -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
		
		
			$keys = $filesFiltered -> keys();
			$key = $files -> search($filesFiltered[$keys[0]]);
		
			
				$lng = count($files);
				if($key == $lng - 1) {
					$fileReturn = $files -> get($key);
				} else {
					$fileReturn = $files -> get($key + 1);
				}
		
				$fileReturn -> viewed = $fileReturn -> viewed + 1;
		
				$fileReturn -> save();
			
				
		
			$array[0] = $fileReturn;
		
			$col = collect($array);
				
				
			switch ($type) {
				case 'images' :
					return view('pages.showImg', ['image' => $col, 'viewed' => $viewed, 'files' => $files]);
					break;
				case 'videos' :
					return view('pages.showVid', ['videos' => $col, 'viewed' => $viewed, 'files' => $files, 'chck' => 1]);
					break;
				case 'music' :
					return view('pages.showMsc', ['musics' => $col, 'viewed' => $viewed, 'files' => $files, 'chck' => 1]);
					break;
			} 
		
		//return "this is " . $id;
		
		
	}	
	
	
	
	
}











