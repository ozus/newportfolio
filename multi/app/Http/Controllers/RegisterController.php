<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
//use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\ActivateUser as ActivateUserNotification;
use App\Events\RegisterUser;

class RegisterController extends Controller
{    
	//use RegistersUsers;
	
	public function __construct() {
		$this -> middleware('guest');
	}
	
	public function getRegister() {
		
		return view('auth.register');
		
	}
	
	public function postRegister(Request $request) {
		
		$this -> validate($request, [
				
			'name' => 'required|unique:users|alpha_num',
			'email' => 'required|unique:users|Email',
			'password' => 'required|alpha_num|Min:2|Max:25',	
				
		]);
		
		$data = array(
				
			'name' => $request -> input('name'),
			'email' => $request -> input('email'),
			'password' => $request -> input('password'),
			'hashed_password' => bcrypt($request -> password),
		);
		
		User::create($data);
		
		$user = User::where('email', '=', $request -> email) -> get();
		
		//$user[0] -> notify(new ActivateUserNotification($user[0] -> id));
		
		event(new RegisterUser($user));
		
			/* $user = User::select('hashed_password') -> where('email', '=', $request -> email) -> get();
			$user[0] -> hashed_password = bcrypt($request -> password);
			$user[0] -> save();
			dd($user); */
			$request -> session() -> flash('success', 'Registration was successfull, you can <a href="auth/login">Login</a>');
			return redirect() -> route('register');
		
	}
}
