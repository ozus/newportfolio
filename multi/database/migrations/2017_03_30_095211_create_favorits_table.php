<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorits', function (Blueprint $table) {
            $table->increments('id');
         	$table -> string('path', 255) -> unique();
         	$table -> string('name', 255) -> unique();
         	$table -> string('extention', 5);
         	$table -> string('desc', 1000);
         	$table -> string('user_name', 255);
         	$table -> string('user', 45);
         	$table -> integer('liked') -> nullable() -> default(NULL);
         	$table -> integer('vewed') -> nullable() -> default(NULL);
         	$table -> integer('disliked') -> nullable() -> default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorits');
    }
}
