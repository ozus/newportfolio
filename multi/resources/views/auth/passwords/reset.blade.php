<html>
	<head>
		<title>Reset password</title>
	</head>
	<body>
		@if(count($errors) >0)
			<ul>
				@foreach($errors -> all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		@endif
		{{session('status')}}
		<form name="form" action="/auth/reset" method="post">
		{{ csrf_field() }}
			<input type="hidden" name="token" value="{{ $token }}">
			<input type="email" name="email" value="{{$email}}"/><br>
			<input type="password" name="password"><br>
			<input type="password" name="password_confirmation" /><br>
			<input type="submit" value="Send" />
		</form>
	</body>
</html>