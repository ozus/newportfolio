<div class="nav">
	<ul>
		<li><a href="{{url('/home')}}">Home</a></li>
		<li><a href="{{url('/upload')}}">Upload</a></li>
		<li><a href="{{url('/image')}}">Images</a></li>
		<li><a href="{{url('/video')}}">Videos</a></li>
		<li><a href="{{url('/document')}}">Files</a></li>
		<li><a href="{{url('/music')}}">Music</a></li>
		<li><a href="{{url('/stats/video')}}">Statistics</a></li>
		
		@if(Auth::check())
			<li class="right"><a href="{{url('/auth/logout')}}">Logout</a></li>
			<li class="right"><a href="{{route('user.profile', ['id' => Auth::user() -> id])}}">{{Auth::user() -> name}}</a>
		@else
			<li class="right"><a href="{{url('/auth/login')}}">Login</a>
			<li class="right"><a href="{{url('/auth/register')}}">Register</a>
		@endif	
	</ul>
</div>
