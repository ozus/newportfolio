@extends('layouts.master')

@section('title')
Uplaod Images, Documents, Videos and Music files
@endsection

@section('content')

<div class = container>
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	
	
	
		<?php $i = 0 ?>
		@foreach($docs as $doc)
			@if($i == 0)
			<div class="row">
			@endif
				<div class="col-md-4 col-sm-3 col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<span>{{$doc -> user_name}}</span>
								@if(Auth::check())
									@if(Auth::user() -> name == $doc -> user)
										<span style="float: right;">
											<a href="{{route('user.profile', ['id' => Auth::user() -> id])}}">{{$doc -> user}}</a>
										</span>
									@else
										<span style="float: right;">
											{{$doc -> user}}
										</span>	
									@endif
								@endif
							</div>
							<div class="panel-body desc-wraper">
								{{$doc -> desc}}
							</div>
							<div class="panel-footer">
								<span><a href="/download/<?php echo $doc -> id; ?>" class="btn btn-default">Download</a></span>
								<span><a href="/show/<?php echo $doc -> id ?>" class="btn btn-default" target="_blank">Preview</a></span>
								@if(Auth::check())
								@if(Auth::user() -> name == $doc -> user)
									<span><a href="/user/edit/<?php echo $doc -> id; ?>" class="btn btn-default">Edit</a></span>
							<span><button type="button" value="<?php echo $doc -> id; ?>" name="<?php echo $doc -> user_name; ?>" class="btn btn-default" onclick="delConfrm(this.value, this.name);">Delete</button></span>	
								@endif
								@endif
							</div>
						</div>
				</div>
			@if($i == 2)
			</div>
			@endif
			@php 
				$i = $i + 1; 
				 if($i > 2) {
				 	$i = 0; 
				}
			@endphp
		@endforeach
	
</div>

<script>
function delConfrm(id, name) {

	
	
	 var y = confirm("Are you sure you want to delete file: " + name);
	 var type = "document";
	if(y == true) {
		//document.getElementById('del').innerHTML = "Video delited";
		window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);

	}
}
</script>



@endsection









