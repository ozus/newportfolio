@extends('layouts.master')

@section('title')
Image Details
@endsection

@section('content')

<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-12">
			<a href="{{url('/paginate')}}/<?php echo $image[0] -> id ?>/images/prev" id="prevv" class="btn btn-primary">Prev</a>
			<span class="pull-right"><a href="{{url('/paginate')}}/<?php echo $image[0] -> id ?>/images/next" id="nextt" class="btn btn-primary">Next</a></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-10">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-10 block-wraper">
					<div class="row title-wraper">
						<span>{{$image[0] -> user_name }}</span>
						<span style="float: right;">
							@if(Auth::check() && Auth::user() -> name == $image[0] -> user)
								<a href="/user/profile/info/<?php echo Auth::user() -> id; ?>" >{{$image[0] -> user}}</a>
							@else
								{{$image[0] -> user}}
							@endif
						</span>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-6">
							<img src="{{url('/')}}/images/<?php echo $image[0] -> name . "." . $image[0] -> extention; ?>"  /></a>   
						</div>			
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-6 desc-wraper">
							{{$image[0] -> desc}}
						</div>	
					</div>
					<div class="row">
						<span><a href="/download/<?php echo $image[0] -> id; ?>" class="btn btn-default">Download</a></span>
						@if(Auth::check())
							@if(Auth::user() -> name == $image[0] -> user)
								<span style="float: right;"><a href="/user/edit/<?php echo $image[0] -> id; ?>" class="btn btn-default">Edit</a></span>
								<span style="float: right;"><a href="/delete/<?php echo $image[0] -> id; ?>" class="btn btn-default">Delete</a></span>
							@endif
						@endif
					</div>
				</div>
			</div>
			<br>
			
			
					
					<div class="row">
						<div id="likeDiv" class="col-md-4 col-lg-4 col-sm-4">
							<button type="button" id="likeButt" class="btn btn-default" onclick="like();">Like image</button>
						</div>
						<div id="disLike" class="col-md-4 col-lg-4 col-sm-4">
							<button type="button" id="dislikeButt" class="btn btn-default" onclick="dislike();">Dislike image</button>
						</div>
						 @if(Auth::check())
						 <?php 
						$filtered = Auth::user() -> favorits -> filter(function($value, $key) use ($image) {
					
				 			return $value -> user_name == $image[0] -> user_name;
					
						});
						?>
							<div id="fav" class="col-md-4 col-lg-4 col-sm-4">
								@if(!$filtered -> isEmpty())
									<button type="button" id="remFavButt" class="btn btn-default" onclick="remFav();">Remove from favorites</button>
								@else
									<button type="button" id="favButt" class="btn btn-default" onclick="addFav();">Add to favorits</button>
								@endif			
							</div>
							<div id="showFav"></div>
								<div id="div2"></div>
						@endif
					</div>
					<br>
					
					<!-- Grading system -->
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
						Grade Image
						</div>
					</div>
					<div id="grades" class="row">
						<div class="col-md-2 col-lg-2 col-sm-2">
							<input id="rad1" type="radio" name="grade" value="1"> 1
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<input id="rad2" type="radio" name="grade" value="2"> 2
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<input id="rad3" type="radio" name="grade" value="3"> 3
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<input id="rad4" type="radio" name="grade" value="4"> 4
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<input id="rad5" type="radio" name="grade" value="5"> 5
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<button  type="button" class="btn btn-primary" onclick="grade();">Grade</button>
						</div>
					</div>
					
					
					
					@if(session('commSuccess'))
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
								{{session('commSuccess')}}
							</div>
						</div>
					@endif
					@if(session('comError'))
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
								{{session('comError')}}
							</div>
						</div>
					@endif
					
			
					
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
						<h3>Comments:</h3>
						@if($image[0] -> comment -> count() > 0)
							@foreach($image[0] -> comment as $com)
								<table class="table table-striped">
									<thead>
										<tr>
											<th>
												<span>{{$com -> author}}</span> 
												<span style="float: right;">{{$com -> time_posted }}||{{$com -> date_posted}}</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												{{$com -> comment}}
											</td>
										</tr>
										@if(Auth::check())
											@if(Auth::user() -> name == $com -> author || Auth::user() -> rights == 'A')
												<tr>
													<td>
														<a href="/comment/delete/<?php echo $com -> id; ?>" class="btn btn-default">Delete</a>
														<a href="/comment/edit/<?php echo $com -> id; ?>" class="btn btn-default">Edit</a>
													</td>	
												</tr>
											@endif
										@endif
									</tbody>
								</table>
								<br>
							@endforeach
							@else 
								Theres no comments yet.
							@endif		
						</div>
					</div>
				
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<h3>Add Comment:</h3>
						<form action="/comment/add" method="get">
							<input type="hidden" name="ID" value="<?php echo $image[0] -> id; ?>" />
							@if(!Auth::check())
								<div class="form-group">
									<label>Your name:</label>
									<input class="form-control" type="text" name="name" /> 
								</div>
							@endif
							<div class="form-group">
								<label>Your Comment:</label>
								<textarea class="form-control" name="comment" rows="10" cols="10"></textarea>
							</div>
							<input type="submit" id="comSubButt" class="btn btn-primary" value="Comment" />
						</form>
					</div>
				</div>
					
					
			</div>
			<div class="col-md-4 col-lg-4 col-sm-4 col-md-push-2 col-lg-push-2 col-sm-push-1">
				<div class="row">
				
					<input type="radio" name="preference" value="viewed" checked="checked" onclick="al()">Most viewed
					<input type="radio" name="preference" value="liked" onclick="al()">Most liked
					<input type="radio" name="preference" value="disliked" onclick="al()">Most disliked
					@if(Auth::check())
						<input type="radio" name="preference" value="favorites" onclick="al()">My favorites
					@endif
					
				</div>
				<div class="row">
					<label id="prefLbl">Most viewed</label>
				</div>
				<div id="pref" class="row">
					
					@foreach($viewed as $view)
						
							<div class="row">
									{{$view -> user_name}}
							</div>
							<div class="row">
								{{$view -> path}}
							</div>
		
						<br>
					@endforeach
				</div>
			</div>
		</div>
				
</div>
@if(session('commSuccess') || session('comError'))
<script>

	window.scrollTo(0, 600);

</script>
@endif

<?php $lng = $files -> count(); 
?>
@if($files -> search($image[0]) == $lng - 1)
	<script>
		document.getElementById('nextt').setAttribute('disabled', 'disabled');
		document.getElementById('nextt').removeAttribute('href');
	</script>
@endif
@if($files -> search($image[0]) == 0)
	<script>
		document.getElementById('prevv').setAttribute('disabled', 'disabled');
		document.getElementById('prevv').removeAttribute('href');
	</script>
@endif

<script>

	function chck(id) {

		var chck = document.getElementById(id);
		
		//chck.setAttribute("checked", "checked");
		//chck.checked = true;
	}

	function delConfrm() {

		var fileName = <?php echo json_encode($image[0] -> user_name); ?>;
		var id = <?php echo json_encode($image[0] -> id); ?>;
		var type = "image";
		
		 var y = confirm("Are you sure you want to delete file: " + fileName);

		if(y == true) {
			//document.getElementById('del').innerHTML = "Video delited";
			window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);
		}
	}

	function al() {

		var pref = document.getElementsByName('preference');
		var lng = pref.length;
		for(var i = 0; i < lng; i++) {

			//var value= pref[i].value;
			
			if(pref[i].checked) {

				var value = pref[i].value

				var lbl = document.getElementById('prefLbl');
				lbl.innerHTML = "";
				lbl.innerHTML = "Most " + value;
				
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						var pr = document.getElementById('pref');
						pr.innerHTML = "";
						pr.innerHTML = this.responseText; 
					}
				}
				xhttp.open("GET", "{{route('image.preference')}}" + "/" + value, true);
				xhttp.send();
				
			}
		}	
		
	}

	function grade() {

		var grades = document.getElementsByName('grade');
		var lng = grades.length;

		var type = "images";
		var id = <?php echo json_encode($image[0] -> id); ?>;

		for(var i = 0; i < lng; i++) {

			if(grades[i].checked) {
				var value = grades[i].value;

				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {

					var div = document.getElementById('grades');
					div.innerHTML = "";
					div.innerHTML = this.responseText;

				}
				xhttp.open("GET", "{{route('user.grade')}}" + "/" + id + "/" + type + "/" + value, true);
				xhttp.send();
			}
		
		}
		
		
	}

	function like() {
		
			var xhttp = new XMLHttpRequest();
			var id = <?php echo json_encode($image[0] -> id); ?>;
			
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
					 var div = document.getElementById('likeDiv');
					div.innerHTML = "";
					div.innerHTML = this.responseText; 
				}
			}
			xhttp.open("GET", "{{route('image.like')}}" + "/" + id, true);
			xhttp.send(); 
		
	}


	function unLike() {

			var xhttp = new XMLHttpRequest();
			var id = <?php echo json_encode($image[0] -> id); ?>;
			
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
	
					var div = document.getElementById('likeDiv');
					div.innerHTML = "";
					div.innerHTML = this.responseText; 
	
				
				}
			}
			xhttp.open("GET", "{{route('image.unlike')}}" + "/" + id, true);
			xhttp.send();
	}


	function dislike() {

		var xhttp = new XMLHttpRequest();
		var id = <?php echo json_encode($image[0] -> id); ?>;
		
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('disLike');
				div.innerHTML = "";
				div.innerHTML = this.responseText; 

			
			}
		}

		xhttp.open("GET", "{{route('image.dislike')}}" + "/" + id, true);
		xhttp.send();

	}

	 function unDislike() {

		var xhttp = new XMLHttpRequest();
		var id = <?php echo json_encode($image[0] -> id); ?>;
		
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('disLike');
				div.innerHTML = "";
				div.innerHTML = this.responseText; 

			
			}
		}
		xhttp.open("GET", "{{route('image.undislike')}}" + "/" + id, true);
		xhttp.send();

	} 


	 function addFav() {

		 var id = <?php echo json_encode($image[0] -> id); ?>;
			var xhttp = new XMLHttpRequest();
				
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {

					var div = document.getElementById('fav');
					div.innerHTML = "";
					div.innerHTML = JSON.parse(this.responseText).first;

					var div2 = document.getElementById('div2');
					div2.innerHTML = "";
					div2.innerHTML = JSON.parse(this.responseText).second;  

				
				}
			}
			xhttp.open("GET", "{{route('user.addFavorite')}}" + "/" + id, true);
			xhttp.send();

	}

/* $(document).ready(function(){
	$('#favButt').click(function() {

	   var request = $.ajax
	    ({
	        method: "POST",
	        url: "{{route('user.addFavorite')}}",
	        dataType: 'html',
	    }) 

	    request.done(function(response) {

	    	$('#showFav').html(response);
	
	    });
	} );	
} ); */

	function remFav() {

		var id = "<?php echo json_encode($image[0] -> id); ?>";
		var xhttp = new XMLHttpRequest();
			
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('fav');
				div.innerHTML = "";
				div.innerHTML = JSON.parse(this.responseText).first;

				var div2 = document.getElementById('div2');
				div2.innerHTML = "";
				div2.innerHTML = JSON.parse(this.responseText).second;  

			
			}
		}
		xhttp.open("GET", "{{route('user.removeFavorite')}}" + "/" + id, true);
		xhttp.send();
	}	
</script>








@endsection