@extends('layouts.master')

@section('title')
Videos
@endsection

@section('content')
@if($chck == 0)
	<input style="display: inline-block; margin-left: 72%" type="checkbox" id="autoplay" name="autop" value="1"> Autoplay
@else
	<input style="display: inline-block; margin-left: 72%" type="checkbox" id="autoplay" name="autop" value="1" checked> Autoplay
@endif

<button id="clcButt" name="{{$videos[0] -> id}}" type="button" onclick="autoChecked(this.name)">Click me</button>

<div class="container">
	<div id="demo"></div>
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	<div id="del"></div>
	
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-12">
			<a href="{{url('/paginate')}}/<?php echo $videos[0] -> id ?>/videos/prev" id="prevv" class="btn btn-primary">Prev</a>
			
			<div style="display: inline-block" class="dropdown">
			  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    Playlist
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			  @foreach($files as $file)
			    <li><a href="{{url('/showVideo')}}/<?php echo $file -> id; ?>">{{$file -> user_name}}</a></li>
			    <li role="separator" class="divider"></li>
			  @endforeach  
			  </ul>
			</div>
			
			<span class="pull-right"><a href="{{url('/paginate')}}/<?php echo $videos[0] -> id ?>/videos/next" id="nextt" class="btn btn-primary">Next</a></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-12">	
			<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12 block-wraper">
						<div class="row title-wraper">
							<span>{{$videos[0] -> user_name}}</span>
							<span style="float: right;">
								@if(Auth::check() && Auth::user() -> name == $videos[0] -> user)
									<a href="{{url('/')}}/user/profile/info/<?php echo Auth::user() -> id; ?>" >{{$videos[0] -> user}}</a>
								@else
									{{$videos[0] -> user}}
								@endif
							</span>
						</div>
						<div class="row">
							
							<div class="embed-responsive embed-responsive-16by9">							
								<video id="myVideo" width="320" height="240" controls="controls">  
								    <source src="{{url('/')}}/videos/<?php echo $videos[0] -> name . "." . $videos[0] -> extention; ?>" type="video/mp4" />   
								    <!-- <track src="subtitles_en.vtt" kind="subtitles" srclang="en" label="English"> -->     
								    Nav doesn't support html5 video
								</video> 
							</div>
							
							{{$videos[0] -> path }}
						</div>
						<div class="row desc-wraper">
							{{$videos[0] -> desc}}
						</div>
						<div class="row">
							<span><a href="{{url('/')}}/download/<?php echo $videos[0] -> id; ?>" class="btn btn-default">Download</a></span>
							@if(Auth::check())
								@if(Auth::user() -> name == $videos[0] -> user)
									<span style="float: right;"><a href="{{url('/')}}/user/edit/<?php echo $videos[0]-> id; ?>" class="btn btn-default">Edit</a></span>
									<span style="float: right;"><button type="button" class="btn btn-default" onclick="delConfrm();">Delete</button></span>
								@endif
							@endif
						</div>
					</div>
			</div>
				<br>
				
				<div class="row">
					<div id="likeDiv" class="col-md-4 col-lg-4 col-sm-4">
						<button type="button" id="likeButt" class="btn btn-default" onclick="like();">Like video</button>
					</div>
					<div id="disLike" class="col-md-4 col-lg-4 col-sm-4">
						<button type="button" id="dislikeButt" class="btn btn-default" onclick="dislike();">Dislike video</button>
					</div>
					 @if(Auth::check())
					 <?php 
					$filtered = Auth::user() -> favorits -> filter(function($value, $key) use ($videos) {
				
			 			return $value -> user_name == $videos[0] -> user_name;
				
					});
					?>
						<div id="fav" class="col-md-4 col-lg-4 col-sm-4">
							@if(!$filtered -> isEmpty())
								<button type="button" id="remFavButt" class="btn btn-default" onclick="remFav();">Remove from favorites</button>
							@else
								<button type="button" id="favButt" class="btn btn-default" onclick="addFav();">Add to favorits</button>
							@endif			
						</div>
						<div id="showFav"></div>
							<div id="div2"></div>
					@endif
				</div>
				<br>
				<!-- Grading system -->
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
					Grade Video
					</div>
				</div>
				<div id="grades" class="row">
					<div class="col-md-2 col-lg-2 col-sm-2">
						<input id="rad1" type="radio" name="grade" value="1" onclick="chck(this.id)"> 1
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2">
						<input id="rad2" type="radio" name="grade" value="2" onclick="chck(this.id)"> 2
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2">
						<input id="rad3" type="radio" name="grade" value="3" onclick="chck(this.id)"> 3
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2">
						<input id="rad4" type="radio" name="grade" value="4" onclick="chck(this.id)"> 4
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2">
						<input id="rad5" type="radio" name="grade" value="5" onclick="chck(this.id)"> 5
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2">
						<button  type="button" class="btn btn-primary" onclick="grade();">Grade</button>
					</div>
				</div>
		</div>
		
		<div class="col-md-4 col-lg-4 col-sm-8 col-md-push-2 col-lg-push-2 col-sm-push-1">
			<div class="row">
				
				<input type="radio" name="preference" value="viewed" checked="checked" onclick="al()">Most viewed
				<input type="radio" name="preference" value="liked" onclick="al()">Most liked
				<input type="radio" name="preference" value="disliked" onclick="al()">Most disliked
				@if(Auth::check())
					<input type="radio" name="preference" value="favorites" onclick="al()">My favorites
				@endif
				
			</div>
			<div class="row">
				<label id="prefLbl">Most viewed</label>
			</div>
			<div id="pref" class="row">
				
				@foreach($viewed as $view)
					
						<div class="row">
								{{$view -> user_name}}
						</div>
						<div class="row">
							{{$view -> path}}
						</div>
	
					<br>
				@endforeach
			</div>
		</div>	
	</div>	
		@if(session('commSuccess'))
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
					{{session('commSuccess')}}
				</div>
			</div>
		@endif
		@if(session('comError'))
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					{{session('comError')}}
				</div>
			</div>
		@endif
		
		
		
		<div class="row">
				<div class="col-md-8 col-lg-8 col-sm-12">
				<h3>Comments:</h3>
				@if($videos[0] -> comment -> count() > 0)
					@foreach($videos[0] -> comment as $com)
						<table class="table table-striped">
							<thead>
								<tr>
									<th>
										<span>{{$com -> author}}</span> 
										<span style="float: right;">{{$com -> time_posted }}||{{$com -> date_posted}}</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										{{$com -> comment}}
									</td>
								</tr>
								@if(Auth::check())
									@if(Auth::user() -> name == $com -> author || Auth::user() -> rights == 'A')
										<tr>
											<td>
												<a href="/comment/delete/<?php echo $com -> id; ?>" class="btn btn-default">Delete</a>
												<a href="/comment/edit/<?php echo $com -> id; ?>" class="btn btn-default">Edit</a>
											</td>	
										</tr>
									@endif
								@endif
							</tbody>
						</table>
						<br>
					@endforeach
					@else 
						Theres no comments yet.
					@endif		
				</div>
			</div>
			
		<div class="row">
			<div class="col-md-8 col-lg-8 col-sm-12">
				<h3>Add Comment:</h3>
				<form action="/comment/add" method="get">
					<input type="hidden" name="ID" value="<?php echo $videos[0] -> id; ?>" />
					@if(!Auth::check())
						<div class="form-group">
							<label>Your name:</label>
							<input class="form-control" type="text" name="name" /> 
						</div>
					@endif
					<div class="form-group">
						<label>Your Comment:</label>
						<textarea class="form-control" name="comment" rows="10" cols="10"></textarea>
					</div>
					<input type="submit" id="comSubButt" class="btn btn-primary" value="Comment" />
				</form>
			</div>
		</div>
	
</div>


<?php $lng = $files -> count();  
?>
@if($files -> search($videos[0]) == $lng - 1)
	<script>
		document.getElementById('nextt').setAttribute('disabled', 'disabled');
		document.getElementById('nextt').removeAttribute('href');
	</script>
@endif
@if($files -> search($videos[0]) == 0)
	<script>
		document.getElementById('prevv').setAttribute('disabled', 'disabled');
		document.getElementById('prevv').removeAttribute('href');
	</script>
@endif

@if($chck == 1) 
	<script>
		var myVideo = document.getElementById('myVideo');
		myVideo.autoplay = true;
	</script>
@endif

<script>

	var video = document.getElementById('myVideo');
	video.onended = function() {
		//document.getElementById('demo').innerHTML = "Songgggggg endedddddddddddddddddddddddddddddd"
		autoChecked();
	}  


	function autoChecked() {

		var id = document.getElementById('clcButt').name;
		var type = "videos";
		var auto = document.getElementsByName('autop');
		if(auto[0].checked) {

			window.location.assign("{{route('page.auto')}}" + "/" + id + "/" + type);

		}	
	}
	
	
	function chck(id) {

		var chck = document.getElementById(id);
		
		//chck.setAttribute("checked", "checked");
		//chck.checked = true;
	}

	function delConfrm() {

		var fileName = <?php echo json_encode($videos[0] -> user_name); ?>;
		var id = <?php echo json_encode($videos[0] -> id); ?>;
		var type = "video";
		
		 var y = confirm("Are you sure you want to delete file: " + fileName);

		if(y == true) {
			//document.getElementById('del').innerHTML = "Video delited";
			window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);
		}
	}

	function al() {

		var pref = document.getElementsByName('preference');
		var lng = pref.length;
		for(var i = 0; i < lng; i++) {

			//var value= pref[i].value;
			
			if(pref[i].checked) {

				var value = pref[i].value

				var lbl = document.getElementById('prefLbl');
				lbl.innerHTML = "";
				lbl.innerHTML = "Most " + value;
				
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						var pr = document.getElementById('pref');
						pr.innerHTML = "";
						pr.innerHTML = this.responseText; 
					}
				}
				xhttp.open("GET", "{{route('video.preference')}}" + "/" + value, true);
				xhttp.send();
				
			}
		}	
		
	}

	function grade() {

		var grades = document.getElementsByName('grade');
		var lng = grades.length;

		var type = "images";
		var id = <?php echo json_encode($videos[0] -> id); ?>;

		for(var i = 0; i < lng; i++) {

			if(grades[i].checked) {
				var value = grades[i].value;

				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {

					var div = document.getElementById('grades');
					div.innerHTML = "";
					div.innerHTML = this.responseText;

				}
				xhttp.open("GET", "{{route('user.grade')}}" + "/" + id + "/" + type + "/" + value, true);
				xhttp.send();
			}
		
		}
		
		
	}

	function like() {
		
			var xhttp = new XMLHttpRequest();
			var id = <?php echo json_encode($videos[0] -> id); ?>;
			
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
					 var div = document.getElementById('likeDiv');
					div.innerHTML = "";
					div.innerHTML = this.responseText; 
				}
			}
			xhttp.open("GET", "{{route('video.like')}}" + "/" + id, true);
			xhttp.send(); 
		
	}


	function unLike() {

			var xhttp = new XMLHttpRequest();
			var id = <?php echo json_encode($videos[0] -> id); ?>;
			
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
	
					var div = document.getElementById('likeDiv');
					div.innerHTML = "";
					div.innerHTML = this.responseText; 
	
				
				}
			}
			xhttp.open("GET", "{{route('video.unlike')}}" + "/" + id, true);
			xhttp.send();
	}


	function dislike() {

		var xhttp = new XMLHttpRequest();
		var id = <?php echo json_encode($videos[0] -> id); ?>;
		
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('disLike');
				div.innerHTML = "";
				div.innerHTML = this.responseText; 

			
			}
		}

		xhttp.open("GET", "{{route('video.dislike')}}" + "/" + id, true);
		xhttp.send();

	}

	 function unDislike() {

		var xhttp = new XMLHttpRequest();
		var id = <?php echo json_encode($videos[0] -> id); ?>;
		
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('disLike');
				div.innerHTML = "";
				div.innerHTML = this.responseText; 

			
			}
		}
		xhttp.open("GET", "{{route('video.undislike')}}" + "/" + id, true);
		xhttp.send();

	} 


	 function addFav() {

		 var id = <?php echo json_encode($videos[0] -> id); ?>;
			var xhttp = new XMLHttpRequest();
				
			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {

					var div = document.getElementById('fav');
					div.innerHTML = "";
					div.innerHTML = JSON.parse(this.responseText).first;

					var div2 = document.getElementById('div2');
					div2.innerHTML = "";
					div2.innerHTML = JSON.parse(this.responseText).second;  

				
				}
			}
			xhttp.open("GET", "{{route('user.addFavorite')}}" + "/" + id, true);
			xhttp.send();

	}

/* $(document).ready(function(){
	$('#favButt').click(function() {

	   var request = $.ajax
	    ({
	        method: "POST",
	        url: "{{route('user.addFavorite')}}",
	        dataType: 'html',
	    }) 

	    request.done(function(response) {

	    	$('#showFav').html(response);
	
	    });
	} );	
} ); */

	function remFav() {

		var id = "<?php echo json_encode($videos[0] -> id); ?>";
		var xhttp = new XMLHttpRequest();
			
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {

				var div = document.getElementById('fav');
				div.innerHTML = "";
				div.innerHTML = JSON.parse(this.responseText).first;

				var div2 = document.getElementById('div2');
				div2.innerHTML = "";
				div2.innerHTML = JSON.parse(this.responseText).second;  

			
			}
		}
		xhttp.open("GET", "{{route('user.removeFavorite')}}" + "/" + id, true);
		xhttp.send();
	}	
</script>


@endsection












