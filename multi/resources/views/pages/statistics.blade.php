@extends('layouts.master')

@section('title')
	<h2>Statistics</h2>
@endsection

@section('content')
	
	<div class="container">
		<div class="row"><h3>{{$title}}</h3></div>
		<div class="row">
			<label>Most Liked</label>
			<div class="col-md-12 col-lg-12 col-sm-12" style="background-color: #E0FFFF">
				<?php echo $liked; ?>
			</div>
		</div>
		<br>
		<div class="row">	
			<label>Most Viewed</label>
			<div class="col-md-12 col-lg-12 col-sm-12" style="background-color: #E0FFFF">
				<?php echo $viewed; ?>
			</div>
		</div>
		<br>
		<div class="row">
			<label>Most Disliked</label>	
			<div class="col-md-12 col-lg-12 col-sm-12" style="background-color: #E0FFFF">
				<?php echo $dis; ?>
			</div>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="row">
					<label>Detailed Statistics</label>
					<span id="showHide">
						<button class="btn btn-default" type="button" onclick="hidee()" >Hide</button>
					</span>	
					 <div id="statsId" class="statstab" >
			 		 	
			   				
			   				<table>
			   					<thead>
			   						<tr>
			   							<th>Name</th>
			   							<th>Viewed</th>
			   							<th>Liked</th>
			   							<th>Disliked</th>
			   							<th>Grade</th>
			   						</tr>
			   					</thead>
			   					<tbody>
			   						@foreach($videos as $video)
			   						<tr>
			   							<td>{{$video -> user_name}}</td>
			   							<td>{{$video -> viewed}}</td>
			   							<td>{{$video -> liked}}</td>
			   							<td>{{$video -> disliked}}</td>
			   							<td>{{$video -> avrg_grade}}</td>
			   						</tr>
			   						@endforeach
			   					</tbody>
			   				</table>
			   				<div class="pull-right">
			   					{{$videos->links()}}
			   				</div>
			  			
					</div>
				</div>	
			</div>
		</div>
			
			
	</div>


<script>
	function hidee() {

		var div = document.getElementById('statsId');
		div.style.display = "none";

		 var xhttp = new XMLHttpRequest();
		 xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
					
					document.getElementById('showHide').innerHTML = "";
					document.getElementById('showHide').innerHTML = this.responseText;
					
				}
			}
			xhttp.open("GET", "{{route('stats.ajaxDetailsHide')}}", true);	
			xhttp.send();
		
	}

	function showw() {

		var div = document.getElementById('statsId');
		div.style.display = "block";

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				
				document.getElementById('showHide').innerHTML = "";
				document.getElementById('showHide').innerHTML = this.responseText;
				
			}
		}
		xhttp.open("GET", "{{route('stats.ajaxDetailsShow')}}", true);	
		xhttp.send();

	}
</script>	
@endsection



