@extends('layouts.master')

@section('title')
Edit File  <b>{{" " . $data[0] -> user_name }}</b>
@endsection

@section('content')

<div class="container">

	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif


	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<form id="myForm" name="form" action="/user/edit" method="post" >
				<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
				<input type="hidden" name="id" value="<?php echo $id ?>" />
				<div class="form-group">
					<label>New name:</label>
					<input type="text" id="nm" class="form-control" name="name" value="<?php echo $data[0] -> user_name ?>" />
				</div>
				<div class="form-group">
					<label>New description:</label>
					<textarea id="ds" rows="" cols="" class="form-control" name="desc" ><?php echo $data[0] -> desc ?></textarea>
				</div>
				<input type="submit" id="subButt" value="Update" class="btn btn-primary"/>
			</form>
		</div>
	</div>
</div>

<script>

/* var form = document.getElementById('myForm');
form.onsubmit = function() {
	//alert('alert');
	var name = document.getElementById('nm');
	name.style.color = "#FFF";
}
function clear() {

	/* var name = document.getElementById('nm');
	name.removeAttribute('value'); */


	/* var desc = document.getElementById('ds');
	desc.removeAttribute('value');
	
} */
</script>

@endsection
