@extends('layouts.master')

@section('title')
Welcome 
@endsection

@section('content')


<div class="container">
	<div class="col-md-3 col-lg-3">
		<div class="alert" id="notify" style="display: none" role="alert">
			<div id="notification"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-10 col-lg-push-1 col-md-push-1">
			<label class="stats-label">Complite Statistics:</label>
			<div class="stats-all-table">
					<table class="table table-responsive" id="contentTable">
						<thead>
							<tr>
								<th>Activity</th>
								<th>Name</th>
								<th>Views</th>
								<th>Likes</th>
								<th>Dislikes</th>
								<th>Grade</th>
								<th>Options</th>
							</tr>	
						</thead>
						<tbody>
							@for($i = 0; $i < $docs -> count(); $i++)
							<tr>
								<td>
									@if($docs[$i] -> shown == 1)
										
										
										<input id="hid{{$docs[$i] -> id}}" name="stats-all" type="hidden" value="0">
										<a href="javascript:void(0)" onclick="return showHide(test<?php echo $docs[$i] -> id; ?>.value, hid<?php echo $docs[$i] -> id; ?>.name);"><span id="stat-all{{$docs[$i] -> id}}" class="activity-label-active"></span></a>
									@else
										
										
										<input id="hid{{$docs[$i] -> id}}" name="stats-all" type="hidden" value="1">
										<a href="javascript:void(0)" onclick="return showHide(test<?php echo $docs[$i] -> id; ?>.value, hid<?php echo $docs[$i] -> id; ?>.name);"><span id="stat-all{{$$docs[$i] -> id}}" class="activity-label-inactive"></span></a>
										
									@endif
								</td>	
								<td>{{$docs[$i] -> user_name}}</td>
								<td>{{$docs[$i] -> viewed}}</td>
								<td>{{$docs[$i] -> liked}}</td>
								<td>{{$docs[$i] -> disliked}}</td>
								<td>{{$docs[$i] -> avrg_grade}}</td>
								<td>
									<input type="hidden" id="test{{ $docs[$i] -> id }}" value="{{ $docs[$i] -> id }}" name="{{ $docs[$i] -> user_name }}" />
									<a href="javascript:void(0)" onclick="return checkDelete(test<?php echo $docs[$i] -> id; ?>.value, test<?php echo $docs[$i] -> id; ?>.name);" ><img title="delete" src="{{url('/')}}/img/trash-icon.png" width="15" height="15" /></a>
									<a href="<?php echo url('/user/edit'); ?>/{{$docs[$i] -> id}}"><img title="edit" src="{{url('/')}}/img/rsz_pencil-256x256.png" width="15" height="15" /></a>
									<a href="{{url('/show')}}/<?php echo $docs[$i] -> id; ?>" target="_blank" ><img title="view" src="{{url('/')}}/img/eye-icon.png" width="15" height="15" /></a>
									
								
									<a href="javascript:void(0)" onclick="return showHide(test<?php echo $docs[$i] -> id; ?>.value);"></a>
								</td>
							</tr>
							@endfor
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>





<script>

function showHide(id, type) {
	
	var disAll = document.getElementById('hid' + id);
			
				
			var value = disAll.value;
			
			if(value == "0") {
				//alert("working");

				document.getElementById('stat-all' + id).setAttribute('class', 'activity-label-inactive');
				disAll.value = "1"
				
			}
			if(value == "1") {
				
				document.getElementById('stat-all' + id).setAttribute('class', 'activity-label-active');
				disAll.value = "0"
					
			} 
			
			var xhttp = new XMLHttpRequest();

			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
					//document.getElementById('demo').innerHTML = this.responseText; 

					$("#notification").html(this.responseText);
					$("#notify").removeClass;
					$("#notify").addClass('alert alert-success');
					$("#notify").show().delay(5000).fadeOut();
				}
			}
			xhttp.open("GET", "{{route('user.showhide')}}" + "/" + id  + "/" + value, true);
			xhttp.send();
}

function checkDelete(id, name) {

	var y = confirm("Are you sure you want to delete file: " + name);
	var type = "image";

	if(y == true) {
		window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);
	}

}

$(document).ready(function() {
    $('#contentTable').DataTable();
});



</script>


@endsection('content')
