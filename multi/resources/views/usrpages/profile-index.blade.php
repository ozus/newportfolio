@extends('layouts.master')

@section('title')
Welcome {{$user[0] -> name}}
@endsection

@section('content')
{!! Html::style('css/Jcrop/jquery.Jcrop.min.css') !!}
<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	


	
			<form  action="{{url('/user/profile/info')}}" method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6 col-lg-6">	
						<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
						<input type="hidden" name="id" value="{{$user[0] -> id}}" >
						<div class="form-group">
							Email: <div class="form-control">{{$user[0] -> email}}</div>
							<input type="hidden" name="email" value="{{$user[0] -> email }}" >
						</div>
						<div class="form-group">
							Rights on platform: <div class="form-control"><?php if($user[0] -> rights == "U") {echo "User";} else {echo "Administrator";} ?> </div> 
						</div>
						<label>To change date input new values in fields below.</label>
						<div class="form-group">
							Name: <input type="text" name="name" class="form-control" value="{{$user[0] -> name}}" />
						</div>
						<div class="form-group">
							Last Name: <input type="text" name="lname" class="form-control" value="{{$user[0] -> lname}}" />
						</div>
						<div class="form-group">
							City: <input type="text" name="city" class="form-control" value="{{$user[0] -> city}}" />
						</div>
						<div class="form-group">
							Country: <input type="text" name="country" class="form-control" value="{{$user[0] -> county}}" />
						</div>
						<div class="form-group">
							Password: <input type="text" name="password" class="form-control" value="{{$user[0] -> password}}" />
						</div>
						<div class="form-group">
							Confirm password: <input type="text" class="form-control" value="{{$user[0] -> password}}" />
						</div>
					</div>
					<div class="col-md-6 col-lg-6 col-md-push-2 col-lg-push-2">			
						<div class="form-group">
							<label>Change Profile picture:</label>
							<input type="file" id="profPic" name="profpic" onchange="readURL(this);" /> 
							<img id="blah" src="#" alt="select image to get started, allowed extentions are jpg and png." />
						</div>
						<div class="form-group">
							<label>Current profile picture:</label></br>
							<?php if(file_exists(public_path('profile/' . $user[0] -> email))) { ?>
								<img src="<?php echo url('/') . "/" . "profile" . "/" . $user[0] -> email . "/" . $user[0] -> profile_pic; ?>" alt="prof pic">
							<?php } else { ?>
								<img src="<?php echo url('/') . "/" . "profile" . "/GenericUser.png"; ?>" alt="prof pic">
							<?php } ?>
						</div>
						
						
						<input type="hidden" size="4" id="x1" name="x1" />
						<input type="hidden" size="4" id="y1" name="y1" />
						<input type="hidden" size="4" id="x2" name="x2" />
						<input type="hidden" size="4" id="y2" name="y2" />
						<input type="hidden" size="4" id="w" name="w" />
						<input type="hidden" size="4" id="h" name="h" />
		
					</div>
				</div>	
				<div class="row">	
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit Changes</button> 
					</div>
				</div>
			</form>		
</div>
{!! HTML::script('js/Jcrop-1902fbc/js/jquery.Jcrop.min.js') !!}
<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .Jcrop({

                	onChange: showCoords,
					onSelect: showCoords,
					aspectRatio: 1 / 1,
					maxSize: [314, 314]
					
                 });
			//document.getElementById('cropInst').innerHTML = "@lang('viahub.profile.cropinst')"
           
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showCoords(c)
{
	jQuery('#x1').val(c.x);
	jQuery('#y1').val(c.y);
	jQuery('#x2').val(c.x2);
	jQuery('#y2').val(c.y2);
	jQuery('#w').val(c.w);
	jQuery('#h').val(c.h);
};
</script>





@endsection	















