@extends('layouts.master')

@section('title')
Welcome {{Auth::user() -> name}}
@endsection

@section('content')

<style>

.modal-body {
	margin-left: 100px;
}

.carousel-inner {
	height: 900px;
}
.carousel-control.left {
  background-image: none;
  background-image: none;
  background-image: none;
  background-image: none;
  filter: ;
  background-repeat: ;
}
.carousel-control.right {
  right: 0;
  left: auto;
  background-image: none;
  background-image: none;
  background-image: none;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
  background-repeat: repeat-x;
}
</style>

<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	<div id="demo"></div>
	<div class="col-md-3 col-lg-3">
		<div class="alert" id="notify" style="display: none" role="alert">
			<div id="notification"></div>
		</div>
	</div>
	
	
	
	<div class="row">
		
		@for($i = 0; $i < $videos -> count(); $i++)
			<div class="modal fade" id="prevFromAll<?php echo $videos[$i] -> id; ?>" role="dialog" >
			  <div class="modal-dialog modal-lg" >
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">{{$videos[$i] -> user_name}}</h4>
			      </div>
			      <div class="modal-body">
			        	
			        	<div class="embed-responsive embed-responsive-16by9">							
							<video id="myVideo" width="320" height="240" controls="controls">  
							<source src="{{url('/')}}/videos/<?php echo $videos[$i] -> name . "." . $videos[$i] -> extention; ?>" type="video/mp4" />   
							<!-- <track src="subtitles_en.vtt" kind="subtitles" srclang="en" label="English"> -->     
							Nav doesn't support html5 video
							</video> 
						</div>
			        	
			        	<div class="form-group">
			        		{{$videos[$i] -> desc}}
			        	</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
		@endfor
		
	</div>
	
	
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="pic-carousel" class="carousel slide" data-interval="10000000" data-ride="carousel">
			
			  <ol class="carousel-indicators">
			  @for($i = 0; $i < $videos -> count(); $i++)
			  	@if($i == 0)
			    	<li data-target="#pic-carousel" data-slide-to="{{$i}}" class="active"></li>
			    @else
			    	<li data-target="#pic-carousel" data-slide-to="{{$i}}"></li>
			    @endif
			  @endfor  
			  </ol>
			
			 <div class="carousel-inner" role="listbox">
				@for($i = 0; $i < $videos -> count(); $i++)
					
					@if($i == 0)
						<div class="item active">
					@else
						<div class="item">
					@endif
							<div class="pull-right">
								@if($videos[$i] -> shown == 1)
									<div id="stat{{$videos[$i] -> id}}" class="activity-label-active"></div>
								@else
									<div id="stat{{$videos[$i] -> id}}" class="activity-label-inactive"></div>
								@endif
							</div>
							<div class="row">
								<div class="col-md-8 col-lg-8 col-md-push-2 col-lg-push-2">
									<div class="stats-media-title">{{$videos[$i] -> user_name}}</div>
									<div class="embed-responsive embed-responsive-16by9">							
										<video id="myVideo" width="320" height="240" controls="controls">  
										    <source src="{{url('/')}}/videos/<?php echo $videos[$i] -> name . "." . $videos[$i] -> extention; ?>" type="video/mp4" />   
										    <!-- <track src="subtitles_en.vtt" kind="subtitles" srclang="en" label="English"> -->     
										    Nav doesn't support html5 video
										</video> 
									</div>
								</div>							
							</div>
							<div class="row">
								<div class="col-md-8 col-lg-8 col-md-push-3 col-lg-push-3">
									<div class="from-group">
										<div class="stats-desc-wraper">
											{{$videos[$i] -> desc}}
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-4 col-lg-4 col-md-push-4 col-lg-push-4">
									<label>Options:</label>
									<div class="form-group">
										<button type="button" class="btn btn-default" value="{{ $videos[$i] -> id }}" name="{{ $videos[$i] -> user_name }}" onclick="checkDelete(this.value, this.name);">Delete</button>
										<a class="btn btn-default" href="<?php echo url('/user/edit'); ?>/{{$videos[$i] -> id}}">Edit</a>
									</div>
								</div>
								
								<div class="col-md-4 col-lg-4 col-md-push-3 col-lg-push-3">
									<label>Show/hide image:</label>
									<div id="select{{$videos[$i] -> id}}">
										@if($videos[$i] -> shown == 1)
											<select class="btn btn-success" name="{{$videos[$i] -> id}}" id="stats" onchange="showHide(this.name, this.id);">
												<option value="1" selected="selected">Show</option>
												<option value="0">Hide</option>
											</select>	
										@else
											<select class="btn btn-danger" name="{{$videos[$i] -> id}}" id="stats" onchange="showHide(this.name, this.id);">
												<option value="1">Show</option>
												<option value="0" selected="selected">Hide</option>
											</select>	
										@endif	
									</div>
								</div>
								
								<div class="col-md-10 col-lg-10 col-lg-push-1 col-md-push-1 ">
									<label>Statistics:</label>
									<div class="profile-stats-table">
										<table>
											<thead>
												<tr>
													<th>Category</th>
													<th>Count</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Views:</td>
													<td>{{$videos[$i] -> viewed}}</td>
												</tr>
												<tr>
													<td>Likes:</td>
													<td>{{$videos[$i] -> liked}}</td>
												</tr>
												<tr>
													<td>Dislikes:</td>
													<td>{{$videos[$i] -> disliked}}</td>
												</tr>
												<tr>
													<td>Grade:</td>
													<td>{{$videos[$i] -> avrg_grade}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
								
							</div>
						</div>				
				@endfor
			</div>
			  <!-- Controls -->
			  <a class="left carousel-control" href="#pic-carousel" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#pic-carousel" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-10 col-lg-push-1 col-md-push-1">
			<label class="stats-label">Complite Statistics:</label><span id="statsHideButt"><button id="showHideStatsButt" type="button" value="hide" class="btn btn-default" onclick="compliteStatsVisibillity(this.value)">Hide</button></span>
			<div id="canHide" style="display: block">
				<div class="stats-all-table">
					<table class="table table-responsive" id="contentTable">
						<thead>
							<tr>
								<th>Activity</th>
								<th>Name</th>
								<th>Description</th>
								<th>Views</th>
								<th>Likes</th>
								<th>Dislikes</th>
								<th>Grade</th>
								<th>Options</th>
							</tr>	
						</thead>
						<tbody>
							@for($i = 0; $i < $videos -> count(); $i++)
							<tr>
								<td>
									@if($videos[$i] -> shown == 1)
										
										
										<input id="hid{{$videos[$i] -> id}}" name="stats-all" type="hidden" value="0">
										<a href="javascript:void(0)" onclick="return showHide(test<?php echo $videos[$i] -> id; ?>.value, hid<?php echo $videos[$i] -> id; ?>.name);"><span id="stat-all{{$videos[$i] -> id}}" class="activity-label-active"></span></a>
									@else
										
										
										<input id="hid{{$videos[$i] -> id}}" name="stats-all" type="hidden" value="1">
										<a href="javascript:void(0)" onclick="return showHide(test<?php echo $videos[$i] -> id; ?>.value, hid<?php echo $videos[$i] -> id; ?>.name);"><span id="stat-all{{$videos[$i] -> id}}" class="activity-label-inactive"></span></a>
										
									@endif
								</td>	
								<td>{{$videos[$i] -> user_name}}</td>
								<td>{{$videos[$i] -> desc}}</td>
								<td>{{$videos[$i] -> viewed}}</td>
								<td>{{$videos[$i] -> liked}}</td>
								<td>{{$videos[$i] -> disliked}}</td>
								<td>{{$videos[$i] -> avrg_grade}}</td>
								<td>
									<input type="hidden" id="test{{ $videos[$i] -> id }}" value="{{ $videos[$i] -> id }}" name="{{ $videos[$i] -> user_name }}" />
									<a href="javascript:void(0)" onclick="return checkDelete(test<?php echo $videos[$i] -> id; ?>.value, test<?php echo $videos[$i] -> id; ?>.name);" ><img title="delete" src="{{url('/')}}/img/trash-icon.png" width="15" height="15" /></a>
									<a href="<?php echo url('/user/edit'); ?>/{{$videos[$i] -> id}}"><img title="edit" src="{{url('/')}}/img/rsz_pencil-256x256.png" width="15" height="15" /></a>
									<a data-toggle="modal" href="#prevFromAll<?php echo $videos[$i] -> id; ?>"><img title="view" src="{{url('/')}}/img/eye-icon.png" width="15" height="15" /></a>
								
									<a href="javascript:void(0)" onclick="return showHide(test<?php echo $videos[$i] -> id; ?>.value);"></a>
								</td>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	function showHide(id, type) {
		var dis = document.getElementsByName(id);
		var disAll = document.getElementById('hid' + id);
				if(type == 'stats') {
					var value = dis[0].value;
				} else {
					var value = disAll.value;
				}
				if(value == "0") {
					var str = "<select class='btn btn-danger' name='" + id + "' id='stats' onchange='showHide(this.name, this.id);'><option value='1'>Show</option><option value='0' selected='selected'>Hide</option></select>";   
					//alert("working");


					document.getElementById('select' + id).innerHTML = str;
					document.getElementById('stat' + id).setAttribute('class', 'activity-label-inactive');
					document.getElementById('stat-all' + id).setAttribute('class', 'activity-label-inactive');
					disAll.value = "1"
					
				}
				if(value == "1") {
					var str = "<select class='btn btn-success' name='" + id + "' id='stats' onchange='showHide(this.name, this.id);'><option value='1' selected='selected'>Show</option><option value='0' >Hide</option></select>";   
					
					document.getElementById('select' + id).innerHTML = str;
					document.getElementById('stat' + id).setAttribute('class', 'activity-label-active');
					document.getElementById('stat-all' + id).setAttribute('class', 'activity-label-active');
					disAll.value = "0"
						
				} 
				
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						//document.getElementById('demo').innerHTML = this.responseText; 

						$("#notification").html(this.responseText);
						$("#notify").removeClass;
						$("#notify").addClass('alert alert-success');
						$("#notify").show().delay(5000).fadeOut();
					}
				}
				xhttp.open("GET", "{{route('user.showhide')}}" + "/" + id  + "/" + value, true);
				xhttp.send();
	}

	function checkDelete(id, name) {

		var y = confirm("Are you sure you want to delete file: " + name);
		var type = "image";

		if(y == true) {
			window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);
		}

	}


	function compliteStatsVisibillity(mode) {

		var div = document.getElementById('canHide');
		var buttSpan = document.getElementById('statsHideButt');
		var butt = document.getElementById('showHideStatsButt');
		
		if(mode == "hide") {
			if(div.style.display == "block") {
				div.style.display = "none";
			}
			butt.value = "show";
			butt.innerHTML = "";
			var z = document.createTextNode('Show'); 
			butt.appendChild(z);	
		}

		if(mode == "show") {
			if(div.style.display == "none") {
				div.style.display = "block";
			}
			butt.value = "hide";
			butt.innerHTML = "";
			var z = document.createTextNode('Hide');
			butt.appendChild(z)	
		}
		
	}


	$(document).ready(function() {
	    $('#contentTable').DataTable();
	});
</script>

@endsection








