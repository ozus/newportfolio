<?php
require_once 'defaults.php';

class Builder {
    
    protected $pdo;
    protected $query;
    protected $table;
    
    protected $encoding;
    protected $host;
    protected $username;
    protected $password;
    protected $db;
    
    protected $defaults;
    
    public function __construct($host=NULL, $db=NULL, $user=NULL, $pass=NULL, $enc=NULL) {
        $def = (new Defaults)->returnDefaults();
        $this->defaults = $def;
        
        $this->encoding = $enc;
        $this->host = $host;
        $this->username = $user;
        $this->password = $pass;
        $this->db = $db;
    }
    
    public function setTable($table) {
        $this->table = $table;
        return $this;
    }
    
   public function setDatabase($base=NULL) {
       if(strlen((string) $base) > 0) {
           $this->db = $base;
       } else {
           if(isset($this->defaults['DB_DATABASE'])) {
               $this->db = $this->defaults['DB_DATABASE'];
           }
       }
       return $this;
   }
    
    public function setHost($host=NULL) {
        if($host) {
            $this->host = $host;
        } else {
            if(isset($this->defaults['DB_HOST'])) {
                $this->host = $this->defaults['DB_HOST'];
            }
        }
        return $this;
    }
    
    public function setUsername($user=NULL) {
        if(strlen((string) $user) > 0) {
            $this->username = $user;
        } else {
            if(isset($this->defaults['DB_USER'])) {
                $this->username = $this->defaults['DB_USER'];
            }
        }
        return $this;
    }
    
    public function setPassword($pass=NULL) {
        if($pass) {
            $this->password = $pass;
        } else {
            if(isset($this->defaults['DB_PASSWORD'])) {
                $this->password = $this->defaults['DB_PASSWORD'];
            }
        }
        return $this;
    }
    
    public function setEncoding($enc=NULL) {
        if(strlen((string) $enc) > 0) {
            $this->encoding = $enc;
        } else {
            if(isset($this->defaults['DB_ENCODING'])) {
                $this->encoding = $this->defaults['DB_ENCODING'];
            }
        }
        return $this;
    }
    
    public function create() {
        try {
           if(isset($this->encoding)) {
               $dns = "mysql:dbname=" . $this->db . ";host=" . $this->host . ";charset=" . $this->encoding;
           } else {
               $dns = "mysql:dbname=" . $this->db . ";host=" . $this->host;
           }
            
            $pdo = new PDO($dns, $this->username, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
            
            
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
    
    public function select(array $columns) {
        //$this -> columns = implode(',', $columns);
        $cols = implode(',', $columns);
        
        $this -> query .= "SELECT " . $cols . " FROM " . $this -> table;
        
        return $this;
    }
    
    public function where($par1, $par2, $par3) {
        
        $string = $this -> makeWhereSring($par1, $par2, $par3);
        
        $this -> query .= " WHERE " . $string;
        
        return $this;
    }
    
    public function orWhere($par1, $par2, $par3) {
        
        $string = $this -> makeWhereSring($par1, $par2, $par3);
        
        $this -> query .= " OR " . $string;
        
        return $this;
    }
    
    public function andWhere($par1, $par2, $par3) {
        
        $string = $this -> makeWhereSring($par1, $par2, $par3);
        
        $this -> query .= " AND " . $string;
        
        return $this;
    }
    
    protected function makeWhereSring($par1, $par2, $forCheck) {
        if(is_string($forCheck)) {
            $lastWithQuotes = "'" . $forCheck . "'";
            $string = $par1 . $par2 . $lastWithQuotes;
        } else {
            $string = $par1 . $par2 . $forCheck;
        }
        
        return $string;
    }
    
    public function insert(array $input) {
        $keys = array_keys($input);
        $values = array_values($input);
        
        $formatedValues = $this->makeInsertString($values);
        
        //var_dump($formatedValues);
        //exit();
        $keysStr = implode(',', $keys);
        
        $this -> query .= "INSERT INTO " . $this -> table . " " . "(" . $keysStr . ") " . " VALUES (" . $formatedValues . ")";
        
        return $this;
    }
    
    public function update(array $input) {
        $string = "";
        
        foreach($input as $column => $value) {
            if(is_string($value)) {
                $value = "'" . $value . "'";
            }
            $string .= $column . "=" . $value . ",";
        }
        
        $string_final = substr($string, 0, strlen($string) - 1);
        
        $this -> query = "UPDATE " . $this -> table . " SET " . $string_final;
        
        return $this;
    }
    
    protected function makeInsertString ($data) {
        $string = "";
        foreach($data as $key => $value) {
            if(is_string($value)) {
                $value = "'" . $value . "'";
            }
            $string .= $value . ",";
        }
        $final_string = substr($string, 0, strlen($string) - 1);
        
        return $final_string;
    }
    
    public function delete() {
        $this->query .= "DELETE FROM " . $this->table;
        return $this;
    }
    
    public function makeItSo() {
        try {
            //var_dump($this -> query);
            //exit();
            $sth = $this -> pdo -> prepare($this -> query);
            $sth -> execute();
        }
        catch(PDOException $e) {
            return $e -> getMessage();
        }
    }
    
    public function getIt() {
        try{
            //var_dump($this -> query);
            //exit();
            $sth = $this -> pdo -> prepare($this -> query);
            $sth -> execute();
            //$this -> getResponse();
            
            $result = $sth -> fetchAll(PDO::FETCH_ASSOC);
            
            return $result;
        }
        catch(PDOException $e) {
            return $e -> getMessage();
        }
    }
    
    
    
    
    
}








