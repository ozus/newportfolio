<?php

class Defaults  {
    
    protected $return = array();
    
    public function __construct() {
        if(file_exists('defaults.txt')) {
            $defaults = file('defaults.txt', FILE_SKIP_EMPTY_LINES);
            if(count($defaults) > 0) {
                foreach ($defaults as $default) {
                  $pices = explode("=", $default);
                  $pices[1] = str_replace("\n", "", $pices[1]);
                  $pices[1] = str_replace("\r", "", $pices[1]);
                    if(isset($pices[0]) && isset($pices[1])) {
                        $this->return[$pices[0]] = $pices[1];
                    }
                }
            }
        }
    }

    public function returnDefaults() {    
        return $this->return;
    }
    
}