<?php

include_once 'Builder.php';

$host = 'localhost';
$db = 'database';
$username = 'root';
$password = '';
$encoding = 'utf8';

//make connection way 1
$builder = new Builder($host, $db, $username, $password, $encoding);
$builder->create();

//make connection way 2
$builder->setHost('localhost')->setDatabase('database')->setPassword()->setUsername('root')->setEncoding('utf8')->create();

//insert statement
$builder->setTable('table')->insert([
    'col1' => "value1",
    'col2' => 'value2',
    'col3' => 'value3',
])->makeItSo();

//update statement
$builder->setTable('table')->update([
    "col1" => 'value1',
    "col2" => "value2",
    "col3" => "value3"
])->where('id', "=", 1)->orWhere('id', "=", 2)->makeItSo();

//select statement
$customer = $builder->setTable('table')->select(['col1', 'col2', '...'])->where('id', "=", 1)->getIt();
var_dump($result);

//delete statement
$builder->setTable('table')->delete()->where('id', "=", 1)->makeItSo();