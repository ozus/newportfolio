<?php namespace App\Forme;

class MakeJson {
	
	protected $json = array();
	protected $messages = array();
	protected $encoded;
	
	
	public function makeSingle($input) {
		
		$this -> json['api_key'] = $input['apykey'];
		$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey']]));
		$this -> json['messages'] = $this -> messages;
		$this -> encodeJson();
		//echo $this -> encoded;
		return $this -> encoded;
	}

	public function makeSingleConcet($input) {
		
		$this -> json['api_key'] = $input['apykey'];
		for($i = 0; $i < $input['times']; $i++) {
			$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey'], 'times' => $input['times']]));
		}
		$this -> json['messages'] = $this -> messages;
		$this -> encodeJson();
		//echo $this -> encoded;
		return $this -> encoded;
	}
	
	
	public function makeMulti($input) {
		$this -> json['api_key'] = $input['apykey'];
		
		$messagesInput = explode(PHP_EOL, trim($input['muiltiMessage']));
		
		foreach($messagesInput as $inputMessage) {
			$parsedInputMessage = str_replace(array("\r\n", "\n", "\r"), "", $inputMessage);
			$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey'], 'muiltiMessage' => $input['muiltiMessage']]), $parsedInputMessage); 
		}
		$this -> json['messages'] = $this -> messages;
		$this -> encodeJson();
		//echo $this -> encoded;
		return $this -> encoded;
		
	}
	
	
	protected function makeMessagesBody($input, $multiMess = NULL) {
		$message = array();
		
		$message['from'] = $input['from'];
		$message['to'] = $input['to'];
		//$message['body'] = $input['singleMessage'];
		$this -> appendMessage($input, $message, $multiMess);
		$this -> checkAndAppendRoute($input['route'], $message);
		$this -> messages[] = $message;
	}
	
	protected function appendMessage($haystack, &$message, $multiMess = NULL) {
		if(isset($haystack['singleMessage'])) {
			$message['body'] = $haystack['singleMessage'];
		} else {
			$message['body'] = $multiMess;
		}
	}
	
	protected function checkAndAppendRoute($route = NULL, &$message) {
		if($route) {
			$message['route'] = $route;
		}
	}
	
	protected function encodeJson() {
		$this -> encoded = json_encode($this -> json);
	}
}
