<?php namespace App\Forme;

class MakeXml {
	
	protected $xml;
	protected $open;
	protected $messages;

	public function init($version, $encoding) {
		$xml = new \DOMDocument($version, $encoding);
		$this -> xml = $xml;
		$open = $this -> xml -> createElement('xml');
		$this -> open = $open;
		
	}
	
	
	public function close() {
		$this -> xml -> appendChild($this -> open);
	}

	public function makeSingle($input) {
		$this -> init('1.0', 'utf-8');
		//dd($input);
		
		$key = $this -> xml -> createElement('api_key', $input['apykey']);
		$this -> open -> appendChild($key);

		$this -> messages = $this -> xml -> createElement('messages');
		$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey']]));
		$this -> open -> appendChild($this -> messages);
		
		$this -> close();
	
		return $this -> xml -> saveXML($this -> xml -> documentElement, LIBXML_NOEMPTYTAG);		
		
		
	}


	public function makeSingleConcet($input)  {
		$this -> init('1.0', 'utf-8');

		$key = $this -> xml -> createElement('api_key', $input['apykey']);
		$this -> open -> appendChild($key);

		$this -> messages = $this -> xml -> createElement('messages');

		for($i=0; $i < $input['times']; $i++) {
		
			$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey'], 'times' => $input['times']]));

		}
		$this -> open -> appendChild($this -> messages);
		
		$this -> close();
		//dd($this -> xml -> saveXML($this -> xml -> documentElement, LIBXML_NOEMPTYTAG));
		return $this -> xml -> saveXML($this -> xml -> documentElement, LIBXML_NOEMPTYTAG);

	}


	public function makeMulti($input) {

		$this -> init('1.0', 'utf-8');

		$key = $this -> xml -> createElement('apikey', $input['apy_key']);
		$this -> open -> appendChild($key);

		$this -> messages = $this -> xml -> createElement('messages');

		$messagesInput = explode(PHP_EOL, trim($input['muiltiMessage']));

		foreach($messagesInput as $inputMessage) {

			$this -> makeMessagesBody(array_diff($input, ['apykey' => $input['apykey'], 'muiltiMessage' => $input['muiltiMessage']]), $inputMessage);

		}
		
		$this -> open -> appendChild($this -> messages);
		
		$this -> close();
		//dd($this -> xml -> saveXML($this -> xml, LIBXML_NOEMPTYTAG));
		return $this -> xml -> saveXML();

	}

	protected function makeMessagesBody($input, $multiMess = NULL) {

		$from = $this-> xml -> createElement('from', $input['from']);
		$to = $this -> xml -> createElement('to', $input['to']);
		
		$message = $this -> xml -> createElement('message');
		$message -> appendChild($from);
		$message -> appendChild($to);
		$this -> appendMessage($input, $message, $multiMess);
		$this -> checkAndAppendRoute($input['route'], $message);	

		$this -> messages -> appendChild($message);
		//$this -> messages = NULL;

	}


	protected function appendMessage($haystack, &$message, $multiMess = NULL) {
		if(isset($haystack['singleMessage'])) {
			$text = $this -> xml -> createElement('body', $haystack['singleMessage']);
		} else {
			$text = $this -> xml -> createElement('body', $multiMess);
		}

		$message -> appendChild($text);
	}

	protected function checkAndAppendRoute($route = NULL, &$message) {
		if(isset($route)) {
			$routeExist = $this -> xml -> createElement('route', $route);
			$message -> appendChild($routeExist);
		}
	}


}
