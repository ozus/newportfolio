<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Number\MakeSingle;
use App\Number\MakeMulti;

class RequestBuildController extends Controller
{
	
	protected $request;
	protected $single;
	protected $multi;
	protected $json;
	protected $xml;
	
	public function __construct(Request $request, MakeSingle $single, MakeMulti $multi) {
		
		$this -> request = $request;
		$this -> single = $single;
		$this -> multi = $multi;
		
	}
	
	
	public function index() {

		//dd($this-> request -> all());
		
		switch($this -> request -> fieldMessNum) {
			case "single" :
				$this -> single -> makeRequest($this -> request -> except(['fieldMessNum']));
				break;
			case "multi" :
				$this -> multi -> makeRequest($this -> request -> except(['fieldMessNum']));
				break;
		}
		
	}
    
}
