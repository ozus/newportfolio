<?php namespace App\Number;

use App\Forme\MakeJson;
use App\Forme\MakeXml;

class MakeMulti {
	
	protected $json;
	protected $xml;
	protected $url;

	public function __construct(MakeJson $json, MakeXml $xml) {
		$this -> json = $json;
		$this -> xml = $xml;
	}

	public function makeRequest($input) {
		
		$this -> url = $input['url'];
		if($input['type'] == "xml") {
			
			$finalRequest = $this -> xml -> makeMulti(array_diff($input, ['type' => 'xml']) );
			$this -> send($finalRequest);
			
		} else {
			$finalRequest = $this -> json -> makeMulti(array_diff($input, ['type' => 'json']) );
		}

	}
	
	
	protected function send($req) {
	
		$ch = \curl_init();
	
		\curl_setopt($ch, CURLOPT_URL, $this -> url);
		\curl_setopt($ch, CURLOPT_POST, 1);
		\curl_setopt($ch, CURLOPT_POSTFIELDS,
				$req
				);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
		$response = \curl_exec($ch);
		$error = curl_error($ch);
	
			
		echo $response . PHP_EOL;
	
	
	}

}
