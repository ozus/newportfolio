<?php namespace App\Number;

use App\Forme\MakeJson;
use App\Forme\MakeXml;

class MakeSingle {
	
	protected $json;
	protected $xml;
	protected $concet = false;
	protected $url;
	
	public function __construct(MakeJson $json, MakeXml $xml) {
		$this -> json = $json;
		$this -> xml = $xml;
	}
	
	public function makeRequest($input) {
		
		$this -> url = $input['url'];
		$this -> isConcet($input['concetOrNot']);

		if($input['type'] == "xml") {
			if($this -> concet) {
				$finalRequest = $this -> xml -> makeSingleConcet(array_diff($input, ['type' => 'xml', 'concetOrNot' => 'concet']) );
				$this -> send($finalRequest);
			} else {
				$finalRequest = $this -> xml -> makeSingle(array_diff($input, ['type' => 'xml', 'concetOrNot' => 'seperate']) );
				$this -> sendSingle($finalRequest, $input['times']);
			}
		} else {
			if($this -> concet) {
				$finalRequest = $this -> json -> makeSingleConcet(array_diff($input, ['type' => 'json', 'concetOrNot' => 'concet']) );
				$this -> send($finalRequest);
			} else {
				$finalRequest = $this -> json -> makeSingle(array_diff($input, ['type' => 'json', 'concetOrNot' => 'seperate']) );
				$this -> sendSingle($finalRequest, $input['times']);
			}
		}
		
	}
	
	protected function isConcet($value) {
		if($value == "concet") {
			$this -> concet = true;
		}
	}
	
	
	protected function sendSingle($singleReq, $times) {
		for($i = 0; $i < $times; $i++) {
			$this -> send($singleReq);
		}
	}
	
	protected function send($req) {
		
		$ch = \curl_init();
		
		\curl_setopt($ch, CURLOPT_URL, $this -> url);
		\curl_setopt($ch, CURLOPT_POST, 1);
		\curl_setopt($ch, CURLOPT_POSTFIELDS, 
			$req		
		);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$response = \curl_exec($ch);
		$error = curl_error($ch);
		
			
		echo $response . PHP_EOL;
	
		
	}
	
}