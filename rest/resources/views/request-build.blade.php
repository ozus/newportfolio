<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>Multimedia site</title>
		{{Html::style('css/bootstrap/bootstrap.css')}}
		{{Html::style('css/custom.css')}}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
		
			@if(count($errors) > 0)
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
								
							<ul>
								@foreach($errors -> all() as $error)
									<li>{{$error}}</li>
								@endforeach	
							</ul>		
						</div>
					</div>
			@endif
			@if(session('success'))
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
						{{session('success')}}
					</div>
				</div>
			@endif
			@if(session('error'))
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
						{{session('error')}}
					</div>
				</div>
			@endif
			
			<div class="row">
				<div class="col-sm-10 col-md-10 col-lg-10">
					<form id="requestForm" action="{{url('build-request')}}" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<label>Url:</label>
							<input type="text" name="url" class="form-control" id="fieldUrl" placeholder="http://example.com" />
						</div>
						<div class="form-group">
							<label>Api Key:</label>
							<input type="text" class="form-control" name="apykey" id="fieldKey" />
						</div>
						<div class="form-group">
							<label>From:</label>
							<input type="text" class="form-control" name="from" id="fieldFrom" placeholder="AlphaNum characters allowed" />
						</div>
						<div class="form-group">
							<label>To:</label>
							<input type="text" class="form-control" name="to" id="fieldTo" placeholder="Only numeric allowed, exmp: 38595..., +38595.." maxlength="16"/>
						</div>
						<div class="form-group">
							<label>Message number:</label>
							<input type="radio" name="fieldMessNum" value="single" checked>Single
							<input type="radio" name="fieldMessNum" value="multi">Multi
						</div>
						<div class="row">
							<div class="col-lg-8 col-sm-8 col-md-8 col-lg-push-1">
								<div id="singleMess">
									<div class="form-group">
										<label>Send times:</label>
										<select name="times">
											@for($i=1; $i < 101; $i++)
												<option value="{{$i}}">{{$i}}</option>
											@endfor
										</select>
										<input type="radio" name="concetOrNot" id="CoN" value="seperate" checked> Seperate
										<input type="radio" name="concetOrNot" id="CoN1" value="concet"> Concet
										<br>
										<label>Message:</label>
										<input type="text" class="form-control" name="singleMessage" id="single" /> 
									</div>
								</div>
								<div id="multiMess" style="display: none">
									<div class="form-group">
										<label>Messages:</label>
										<textarea rows="10" cols="20" class="form-control" 	name="muiltiMessage" id="multi" placeholder="Seperate messages with new line"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Route:</label>
							<select id="routeSelect" name="route" class="form-control">
								<option value=""></option>
								<option value="basic">Basic</option>
								<option value="silver">Silver</option>
								<option value="gold">Gold</option>
							</select>
						</div>
						
						<div class="form-group">
							<label>Request Type:</label>
							<select id="typeSelect" name="type" class="form-control">
								<option value="json">Json</option>
								<option value="xml">Xml</option>
							</select>
						</div>
						<input type="submit" value="Send Request" class="btn btn-primary" />
					</form>
				</div>
			</div>
			
			
		</div>
		{{Html::script('js/bootstrap/bootstrap.min.js')}}
	
	<script>
		var messNum = document.getElementsByName('fieldMessNum');
		//alert(messNum.length);
		for(var i = 0; i < messNum.length; i++) {
			messNum[i].onchange = function() {
				if(this.checked) {
					if(this.value == "multi") {
						document.getElementById('multiMess').style.display = "block";
						document.getElementById('singleMess').style.display = "none";
						document.getElementById('single').value = "";
					}
					if(this.value == "single") {
						document.getElementById('multiMess').style.display = "none";
						document.getElementById('singleMess').style.display = "block";
						document.getElementById('multi').value = "";
					}
				}
			}
		}
		
	</script>
	
	</body>
	
	
	
	