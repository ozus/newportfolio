<?php

class Calculate
{
	
	protected static $solution;
	protected static $tempSolution = 0;
	protected $nums;
	protected $sighns;
	protected $devZerro = 0;
	
	
	public function __construct($n, $s) {
		
		$this -> nums = $n;
		$this -> sighns = $s;
		
	}
	
	public function operate() {
	
		$sig = array('*', "/");
		
		for($i = 0 ; $i < count($this -> sighns); $i++) {
			
			
				while(in_array($this -> sighns[$i], $sig)) {
				
					if($this -> sighns[$i] == "*") {
						
						if(self::$tempSolution == 0) {
							self::$tempSolution = $this -> nums[$i];
							unset($this -> nums[$i]);
							
						}
						
						$this -> multiplie($this -> nums[$i + 1]);
						unset($this -> nums[$i + 1]);
						
						
					}
					
					if($this -> sighns[$i] == "/") {
						if(self::$tempSolution == 0) {
							self::$tempSolution = $this -> nums[$i];
							unset($this -> nums[$i]);
							
						}
						
						$this -> devide($this -> nums[$i + 1]);
						unset($this -> nums[$i + 1]); 	
						
					} 
					
					unset($this -> sighns[$i]);
				
					$i++;	
						
					if($i >= count($this -> sighns)) {
						break;
					}
				
				}
			
			if(self::$tempSolution != 0) {
			//array_push($temps, self::$tempSolution);
			$this -> nums[$i] = self::$tempSolution;
			self::$tempSolution = 0;
			}

		
		}
		
		ksort($this -> nums);
		ksort($this -> sighns);
		$resNums = array_values($this -> nums);
		$resSighns = array_values($this -> sighns);
		
		
		self::$solution = $resNums[0];
		for($j = 0; $j < count($resSighns); $j++) {
		
		
			if($resSighns[$j] == "+") {
					
				$this -> add($resNums[$j + 1]);
					
			}
		
			if($resSighns[$j] == "-") {
					
				$this -> substract($resNums[$j + 1]);
					
			}
		
		}
		
		if($this -> devZerro == 0) {
			return self::$solution;
		} else {
			return "Devision by zero";
		}
	}
	
	protected function add($num) {
		self::$solution = self::$solution + $num;
	}
	
	protected function substract($num) {
		self::$solution = self::$solution - $num;
	}
	
	protected function multiplie($num) {
		self::$tempSolution = self::$tempSolution * $num;
	}
	
	protected function devide($num) {
		if($num != 0) {
			self::$tempSolution = self::$tempSolution / $num;
		} else {
			$this -> devZerro = 1;
		}
	}
	
	
}
















