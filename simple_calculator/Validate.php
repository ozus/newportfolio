<?php

include 'exceptions/SynException.php';
include 'exceptions/MaeException.php';

class Validate
{
	
	public function validateInput(array $data) {
						
			
			$this -> synValidate($data[0]);
			$this -> maValidate($data[0]);
			
	} 
	
	
	protected function synValidate($data) {
		
		if(in_array(null, $data)) {
			throw new SynException("Syn Error");
		}
		
	}
		
	protected function maValidate($data) {
		
		foreach($data as $dat) {
			if(!is_numeric($dat)) {
				throw new MaException("Not numeric");
			}
		}
		
	}	
	
	
}
