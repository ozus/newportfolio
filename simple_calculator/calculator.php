<!DOCTYPE html>
<html>
	<head>
		<title>Simple Calculator</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta charset="UTF-8">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
		<link href="" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		
		<input readonly type="text" id="forCalculate" class="form-control" name="calculate" />
		
		<button type="button" value="0" name="numbers" class="btn btn-default" >0</button> 
		<button type="button" value="1" name="numbers" class="btn btn-default" >1</button> 
		<button type="button" value="2" name="numbers" class="btn btn-default" >2</button>
		<button type="button" value="3" name="numbers" class="btn btn-default" >3</button>
		<button type="button" value="4" name="numbers" class="btn btn-default" >4</button>
		<button type="button" value="5" name="numbers" class="btn btn-default" >5</button>
		<button type="button" value="6" name="numbers" class="btn btn-default" >6</button>
		<button type="button" value="7" name="numbers" class="btn btn-default" >7</button>
		<button type="button" value="8" name="numbers" class="btn btn-default" >8</button>
		<button type="button" value="9" name="numbers" class="btn btn-default" >9</button>
	
		<br><br>
		
		<button type="button" value="+" name="sighns" class="btn btn-primary" >+</button>
		<button type="button" value="-" name="sighns" class="btn btn-primary" >-</button>
		<button type="button" value="*" name="sighns" class="btn btn-primary" >*</button>
		<button type="button" value="/" name="sighns" class="btn btn-primary" >/</button>
		
		<br><br>
		
		<button type="button" id="submit" class="btn btn-warning" >=</button>
		
		<br><br>
	
		<button type="button" id="clear" class="btn btn-danger" >C</button>
		<button type="button" id="delete" class="btn btn-danger" >DEL</button>
	
		<div id="demo"></div>
	
	
	
		<script>
			
			var numbersArray = [];
			var sighnsArray =  [];
			var sighnsArraySend = [];
			var forCalculate = document.getElementById('forCalculate');
			var numbers = document.getElementsByName('numbers');
			var sighns = document.getElementsByName('sighns');
			
			var clearButt = document.getElementById('clear');
			var dellButt = document.getElementById('delete');
			var subButt = document.getElementById('submit');
			
				var numLng = numbers.length;
				for(var i = 0; i < numLng; i++) {

					numbers[i].onclick = function() {

						forCalculate.value = forCalculate.value + this.value;
						
					}

				}

				var sighnLng = sighns.length
				for(var j = 0; j < sighnLng; j++) {

					sighns[j].onclick = function() {
						
							sighnsArraySend.push(encodeURIComponent(this.value));
							sighnsArray.push(this.value);
						
						var sighnsInArray = sighnsArray.length;
						if(sighnsInArray > 1) {
							var index = forCalculate.value.lastIndexOf(sighnsArray[sighnsInArray - 2]);
							var num = forCalculate.value.substring(index + 1, forCalculate.value.length);
							numbersArray.push(num);
						} else {
							numbersArray.push(forCalculate.value);
						}
						forCalculate.value = forCalculate.value + this.value;
						
					}
					
				}

				clearButt.onclick = function() {

					numbersArray = [];
					sighnsArray =  [];
					sighnsArraySend = [];
					forCalculate.value = "";
					

				}


				dellButt.onclick = function() {

					var lastChar = forCalculate.value.substring(forCalculate.value.length - 1, forCalculate.value.length)
					var sighnsInArrayNow = sighnsArray.length;
					if(lastChar == "+" || lastChar == "-" || lastChar == "*" || lastChar == "/") {
						sighnsArray.pop();
						sighnsArraySend.pop();
						
					}  else {
						if(sighnsInArrayNow > 0) {
							var index1 = forCalculate.value.lastIndexOf(sighnsArray[sighnsInArrayNow - 1]);
							var num1 = forCalculate.value.substring(index1 + 1, forCalculate.value.length);
							 if(numbersArray.includes(num1)) {
								numbersArray.pop();
							} 
						} else {
							if(numbersArray.includes(forCalculate.value)) {
								numbersArray.pop();
							}
						}
						
					} 

					
					
					forCalculate.value = forCalculate.value.substring(0, forCalculate.value.length - 1);

				}
					

				subButt.onclick = function() {

					var sighnsInArrayFinal = sighnsArray.length;
					var index2 = forCalculate.value.lastIndexOf(sighnsArray[sighnsInArrayFinal - 1]);
					var num2 = forCalculate.value.substring(index2 + 1, forCalculate.value.length);
					numbersArray.push(num2);

					var nums = JSON.stringify(numbersArray);
					var sighs = JSON.stringify(sighnsArraySend);


					var xhttp = new XMLHttpRequest();

					xhttp.onreadystatechange = function() {
						if(this.readyState == 4 && this.status == 200) {
							
							document.getElementById('demo').innerHTML = this.responseText;
							forCalculate.value = this.responseText;
						}
					}
					xhttp.open("GET", "work.php?numbers=" + nums + "&sighns=" + sighs, true);
					xhttp.send();

					numbersArray = [];
					sighnsArray =  [];
					sighnsArraySend = [];
					
				}
				
				

		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
	
	</body>
</html>