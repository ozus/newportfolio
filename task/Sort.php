<?php

class Sort {
	
	
	public function sort($matrix) {
		$array1 = $matrix[1];
		$array2 = $matrix[2];
		$array3 = $matrix[3];
		
		array_multisort($array1, $array2, $array3);
		
		return array($array1, $array2, $array3);
	}
}
