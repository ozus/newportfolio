<?php

class ValidateMatrix {
	
	protected $error = 0;

		
	public function validate($matrix) {
		
		for($i =1; $i < count($matrix); $i++) {
			for($j = 1; $j < count($matrix); $j++) {
				
				$this -> length($matrix[$i][$j]);
				$this -> type($matrix[$i][$j]);
				$this -> isEmpty($matrix[$i][$j]);
			}
		}
		
		return $this -> error;
	}
	
	
	protected function length($element) {
		
		$stringElement = (string)$element; 
		$length = strlen($stringElement);
		if($length > 1) {
			$this -> error = 1;
		}
	}
	
	protected function type($element) {

		if(!is_numeric($element)) {
			$this -> error = 1;
		}
		
	}
	
	protected function isEmpty($element) {
		
		if(empty($element)) {
			$this -> error = 1;
		}
	}
	
}