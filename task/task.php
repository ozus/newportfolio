<html>
	<head>
		<title>Find a solution</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
		<link href="css/Main.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
	
	<div class="wraper">
	
		<div class="row">
			<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-3 col-xs-pull-3 col-sm-push-1">
				<div class="task-wraper">
					<label>Task:</label>
					<div class="form-group">
						Insert values in grid belove, every value must be a single digit number between 1 and 9. First click Sort Matrix button to sort values by firrst line in asscendig order. After you do that, click on Find Soulution button to calculate the sum of keys, that represent position of largest number or numbers, in sorted matrix.
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-lg-2 col-sm-8 col-xs-8 col-md-push-5 col-lg-push-5 col-xs-push-1 col-sm-push-6">
				
				<div id="frontVal" class="front-val"></div>
			
				<label>Input values for a matrix: </label>
				<div class="form-group">
					<input type="text" name="matrix" />
					<input type="text" name="matrix" />
					<input type="text" name="matrix" />
				</div>
				<div class="form-group">
					<input type="text" name="matrix" />
					<input type="text" name="matrix" />
					<input type="text" name="matrix" />
				</div>
				<div class="form-group">
					<input type="text" name="matrix" />
					<input type="text" name="matrix" />
					<input type="text" name="matrix" /><br>
				</div>
				<div class="buttons">
				<button type="button" class="btn btn-primary" onclick="sortMatrix()">Sort Matrix</button>
				<button type="button" class="btn btn-primary" onclick="clearMatrix()">Clear Matrix</button>
  				</div>
		  		<div id="hiddenDiv" style="display: none">
					<div class="sorted-table">
					<label>Sorted Matrix:</label>
						<table id="sortTable">
									
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
									
						</table>
					</div>
					<button type="button" class="btn btn-primary" onclick="findSolutin()">Find Solution</button>	
				</div>
				<div id="backVal" style="color: red;"></div>
				<div id="solutionDiv" style="display: none">	
					<div class="soulution-wraper">
						<div class="inner-wraper">
							<div class="solution-content">
								<div id="solution"></div>
							</div>
						</div>
					</div>		
				</div>		
					
			</div>
		</div>
	</div>	

				
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
		<script>

			window.onblur = function() {
				document.title = "Hey come back!";
			}

			window.onfocus = function() {
				document.title = "Find a solution";
			}
		
			var elements = document.getElementsByName('matrix');
			var lng = elements.length;
			var errorDiv = document.getElementById('frontVal')
			
			for(var i = 0; i < lng; i++) {
				

				elements[i].onblur = function() {

					if(this.value.length > 1) {
						this.setAttribute('class', 'error');
						errorDiv.setAttribute('class', 'alert alert-danger');
						errorDiv.innerHTML = "Pleas use only one digit number";
					}
					if(isNaN(this.value)) {
						this.setAttribute('class', 'error');
						errorDiv.setAttribute('class', 'alert alert-danger');;
						errorDiv.innerHTML = "input must be a number";
					}
					 if(this.value == 0) {
						this.setAttribute('class', 'error');
						errorDiv.setAttribute('class', 'alert alert-danger');;
						errorDiv.innerHTML = "0 is not a valid input!";
					} 
					if(this.value.length < 1) {
						this.setAttribute('class', 'error');
						errorDiv.setAttribute('class', 'alert alert-danger');
						errorDiv.innerHTML = "field can not be empty";
					}
					if(this.value.length == 1 && !isNaN(this.value) && this.value != 0) {
						errorDiv.innerHTML = "";
						errorDiv.removeAttribute('class');
						this.removeAttribute('class');
					} 
		
				}
			}

			function sortMatrix() {

				if(document.getElementById('solutionDiv').style.display == "block") {
					document.getElementById('solutionDiv').style.display = "none";
				}
				
				var array1 = [];
				var array2 = [];
				var array3 = [];
				var matrix = [];

				for(var i = 1; i <= 3; i++) {
					array1[i] = elements[i-1].value;
				}
				for(var i = 4; i <= 6; i++) {
					array2[i - 3] = elements[i-1].value;
				}
				for(var i = 7; i <= 9; i++) {
					array3[i - 6] = elements[i-1].value;
				}


				matrix[1] = array1;
				matrix[2] = array2;
				matrix[3] = array3;
				
				var matrixSend = JSON.stringify(matrix); 

				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						var deligate = JSON.parse(this.responseText)[1];
						if(deligate == 0) { 
							document.getElementById('solution').innerHTML = JSON.parse(this.responseText)[0];
							document.getElementById('backVal').innerHTML = "";


							document.getElementById('hiddenDiv').style.display = "block";
							var tab = document.getElementById('sortTable');

							var rowsNum = tab.rows.length;

							for(var i = 0; i < tab.rows.length; i++) {

								for(var j = 0; j < tab.rows[i].cells.length; j++) {
									tab.rows[i].cells[j].innerHTML = JSON.parse(this.responseText)[2][i+1][j+1];
								}

							}
								
						} 
						if(deligate == 1) {
							document.getElementById('backVal').innerHTML = JSON.parse(this.responseText)[0]
							document.getElementById('solution').innerHTML = "";
						}
						
					}
				}
				xhttp.open("GET", "findSolution.php?matrix=" + matrixSend, true);
				xhttp.send();	 

				
			} 

			function clearMatrix() {
				for(var i = 0; i < lng; i++) {
					elements[i].value = "";
				}
			}
			
			function findSolutin() {
					document.getElementById('solutionDiv').style.display = "block";
			}
		</script>
		
	</body>
</html>
